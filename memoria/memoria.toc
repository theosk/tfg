\select@language {spanish}
\contentsline {chapter}{\numberline {1}Resumen}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Introduction}{5}{section.1.1}% 
\contentsline {section}{\numberline {1.2}The ECS Pattern}{7}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Composition vs Inheritance}{8}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}What is ECS: Components}{8}{subsection.1.2.2}% 
\contentsline {subsection}{\numberline {1.2.3}What is ECS: Entities}{8}{subsection.1.2.3}% 
\contentsline {subsection}{\numberline {1.2.4}What is ECS: Systems}{8}{subsection.1.2.4}% 
\contentsline {subsection}{\numberline {1.2.5}What is ECS: beyond entities, components and systems}{9}{subsection.1.2.5}% 
\contentsline {section}{\numberline {1.3}An example program}{9}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Goals}{11}{section.1.4}% 
\contentsline {subsection}{\numberline {1.4.1}Language specification}{11}{subsection.1.4.1}% 
\contentsline {subsection}{\numberline {1.4.2}Prototype implementation}{11}{subsection.1.4.2}% 
\contentsline {subsection}{\numberline {1.4.3}Prototype evaluation}{11}{subsection.1.4.3}% 
\contentsline {chapter}{\numberline {2}Introducción}{13}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Contexto}{13}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Introducción a ECS: mundo de ejemplo}{15}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Composición frente a herencia}{16}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Qué es ECS: Componentes}{17}{subsection.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Qué es ECS: Entidades}{17}{subsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.2.4}Qué es ECS: Sistemas}{18}{subsection.2.2.4}% 
\contentsline {subsection}{\numberline {2.2.5}Qué es ECS: más allá de entidades, componentes y sistemas}{18}{subsection.2.2.5}% 
\contentsline {section}{\numberline {2.3}¿Qué entendemos por lenguaje de scripting en el contexto de los videojuegos?}{18}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Arquitectura básica de un videojuego}{19}{section.2.4}% 
\contentsline {chapter}{\numberline {3}Estado del arte}{21}{chapter.3}% 
\contentsline {section}{\numberline {3.1}LUA}{21}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Ventajas}{21}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Inconvenientes}{22}{subsection.3.1.2}% 
\contentsline {section}{\numberline {3.2}Otros lenguajes de script}{22}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Squirrel}{22}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Moonscript}{22}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Haxe}{23}{subsection.3.2.3}% 
\contentsline {section}{\numberline {3.3}Entornos de desarrollo con lenguajes propios}{23}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Unreal Engine 4: blueprints}{23}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Godot Engine: GDScript}{23}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}GameMaker: GML}{24}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}Unity: C\# y javascript}{24}{subsection.3.3.4}% 
\contentsline {subsection}{\numberline {3.3.5}Entornos de desarrollo con lenguajes propios: Conclusiones}{24}{subsection.3.3.5}% 
\contentsline {section}{\numberline {3.4}Otras soluciones específicas}{24}{section.3.4}% 
\contentsline {chapter}{\numberline {4}Análisis de objetivos y metodología}{25}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Análisis de objetivos}{25}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Metodología}{25}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Especificación del lenguaje}{26}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Implementación del prototipo}{26}{subsection.4.2.2}% 
\contentsline {subsection}{\numberline {4.2.3}Evaluación del prototipo}{26}{subsection.4.2.3}% 
\contentsline {chapter}{\numberline {5}Diseño y resolución del trabajo realizado}{27}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Visión de alto nivel del lenguaje}{27}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Descripción general}{27}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}Ejemplo de programa}{27}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}Construcciones facilitadas por el lenguaje}{28}{subsection.5.1.3}% 
\contentsline {subsubsection}{\nonumberline Componentes}{28}{section*.4}% 
\contentsline {subsubsection}{\nonumberline Entidades}{28}{section*.5}% 
\contentsline {subsubsection}{\nonumberline Sistemas}{28}{section*.6}% 
\contentsline {subsubsection}{\nonumberline Funciones}{29}{section*.7}% 
\contentsline {subsection}{\numberline {5.1.4}Flujo de ejecución de un programa}{29}{subsection.5.1.4}% 
\contentsline {section}{\numberline {5.2}Especificación del lenguaje}{29}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Identificadores}{29}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Palabras clave}{30}{subsection.5.2.2}% 
\contentsline {subsection}{\numberline {5.2.3}Tipos de datos}{30}{subsection.5.2.3}% 
\contentsline {subsection}{\numberline {5.2.4}Componentes}{30}{subsection.5.2.4}% 
\contentsline {subsection}{\numberline {5.2.5}Entidades}{30}{subsection.5.2.5}% 
\contentsline {subsection}{\numberline {5.2.6}Sistemas}{30}{subsection.5.2.6}% 
\contentsline {subsection}{\numberline {5.2.7}Funciones}{31}{subsection.5.2.7}% 
\contentsline {subsection}{\numberline {5.2.8}Literales}{31}{subsection.5.2.8}% 
\contentsline {subsection}{\numberline {5.2.9}Variables}{31}{subsection.5.2.9}% 
\contentsline {subsubsection}{\nonumberline Definición e inicialización}{32}{section*.8}% 
\contentsline {subsubsection}{\nonumberline Acceso y alcance}{32}{section*.9}% 
\contentsline {subsection}{\numberline {5.2.10}Operadores}{33}{subsection.5.2.10}% 
\contentsline {subsubsection}{\nonumberline Operadores aritméticos}{33}{section*.10}% 
\contentsline {subsubsection}{\nonumberline Comparadores}{33}{section*.11}% 
\contentsline {subsubsection}{\nonumberline Operadores unarios}{33}{section*.12}% 
\contentsline {subsection}{\numberline {5.2.11}Estructuras de control}{33}{subsection.5.2.11}% 
\contentsline {subsubsection}{\nonumberline if/else}{33}{section*.13}% 
\contentsline {subsubsection}{\nonumberline while}{34}{section*.14}% 
\contentsline {subsection}{\numberline {5.2.12}Elementos predefinidos en el lenguaje}{34}{subsection.5.2.12}% 
\contentsline {subsubsection}{\nonumberline Componente Sprite}{34}{section*.15}% 
\contentsline {subsubsection}{\nonumberline Sistema de dibujo}{34}{section*.16}% 
\contentsline {subsubsection}{\nonumberline Sistema de manejo de ratón}{34}{section*.17}% 
\contentsline {subsubsection}{\nonumberline Funciones predefinidas: funciones matemáticas}{34}{section*.18}% 
\contentsline {subsubsection}{\nonumberline Funciones predefinidas: funciones de sistema}{35}{section*.19}% 
\contentsline {subsection}{\numberline {5.2.13}Especificación formal}{35}{subsection.5.2.13}% 
\contentsline {section}{\numberline {5.3}Implementación y herramientas}{38}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}C++, Flex y Bison}{38}{subsection.5.3.1}% 
\contentsline {subsubsection}{\nonumberline Estructuras de datos del intérprete}{38}{section*.20}% 
\contentsline {subsubsection}{\nonumberline Árboles de Sintaxis Abstracta}{39}{section*.21}% 
\contentsline {subsection}{\numberline {5.3.2}SFML}{41}{subsection.5.3.2}% 
\contentsline {subsection}{\numberline {5.3.3}Prototipo desarrollado: repositorio de código}{41}{subsection.5.3.3}% 
\contentsline {chapter}{\numberline {6}Conclusiones y vías futuras}{43}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Conclusiones}{43}{section.6.1}% 
\contentsline {subsection}{\numberline {6.1.1}Implementación sencilla de ECS}{43}{subsection.6.1.1}% 
\contentsline {subsection}{\numberline {6.1.2}Subsistema gráfico sencillo}{43}{subsection.6.1.2}% 
\contentsline {subsection}{\numberline {6.1.3}Un lenguaje lo suficientemente potente pese a su simplicidad}{43}{subsection.6.1.3}% 
\contentsline {subsection}{\numberline {6.1.4}Potencial de mejora}{44}{subsection.6.1.4}% 
\contentsline {section}{\numberline {6.2}Vías futuras}{44}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Funcionalidad adicional proporcionada por el lenguaje}{44}{subsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.2.2}Subsistema de sonido}{44}{subsection.6.2.2}% 
\contentsline {subsection}{\numberline {6.2.3}Subsistema de entrada/salida}{44}{subsection.6.2.3}% 
\contentsline {subsection}{\numberline {6.2.4}Herramientas de generación de código}{45}{subsection.6.2.4}% 
\contentsline {subsection}{\numberline {6.2.5}Integrar otros frameworks y librerías gráficas}{45}{subsection.6.2.5}% 
\contentsline {chapter}{\nonumberline Bibliografía}{47}{chapter*.22}% 
\contentsline {chapter}{\numberline {A}Anexo I - Ejemplos prácticos del lenguaje}{49}{appendix.A}% 
\contentsline {section}{\numberline {A.1}Tutorial 01 - Mostrando una imagen en pantalla}{49}{section.A.1}% 
\contentsline {section}{\numberline {A.2}Tutorial 02 - Definiendo sistemas y componentes propios}{51}{section.A.2}% 
\contentsline {section}{\numberline {A.3}Tutorial 03 - Uso del sistema predefinido "mouse"}{56}{section.A.3}% 
