%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plantilla TFG/TFM
% Universidad de Murcia. Facultad de Informática
% Realizado por: José Manuel Requena Plens
% Modificado: Pablo José Rocamora Zamora
% Contacto: pablojoserocamora@gmail.com
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\chapter{Introducción}


\section{Contexto}
El momento actual es probablemente el mejor para el desarrollo de videojuegos y aplicaciones multimedia independientes. Contamos con más herramientas que nunca, aptas para todos los niveles y plataformas, y existen soluciones de gran calidad que son libres desde su concepción, como es el caso de Godot Engine, u otras que han eliminado la barrera de entrada ofreciendo ediciones gratuitas de sus productos, como es el ejemplo de suites y motores como Unity o Unreal Engine, que tradicionalmente requerían licencias casi prohibitivas en todas sus versiones.
\\

Más allá de las grandes soluciones y herramientas para no programadores, existen frameworks y librerías como SDL, Allegro o Irrlich3d, entre otros, que se han adaptado a las plataformas y tecnologías actuales, ofreciendo a los programadores bindings en casi cualquier lenguaje para sus desarrollos, resolviendo la implementación de funciones de sonido, gráficos y entrada/salida.
\\

Finalmente, contamos con herramientas más sencillas o curiosas, como es el caso de pico8, una máquina ficticia que imita el hardware de las antiguas consolas de 8 bits, para la que se han desarrollado emuladores y entornos de desarrollo integrados.
\\

El desarrollo de juegos sencillos y de sus mecánicas quedan cubiertos con todas estas herramientas. Pero cuando queremos introducir animaciones, eventos que siguen un guión, u otros elementos generalmente pasivos menos interactivos que se salen del flujo normal del juego, nos formulamos las siguientes preguntas: ¿es correcto ensuciar el código del juego para introducir todo esto? ¿podríamos recurrir a alguna librería de animaciones o vídeo? ¿y si se quiere que estos eventos se comporten de otro modo según el estado del programa?
\\

La respuesta habitual son lenguajes de script como LUA, que cuentan con bindings para los lenguajes más habituales (C++, Java, etc). Se trata de lenguajes completos, con sus pros y sus contras, que son interpretados en tiempo de ejecución y permiten mapear funciones del lenguaje anfitrión (en un código en C se podrían exponer algunas funciones y mapearlas a funciones del lenguaje de script, resolviendo el problema de la interacción entre los scripts y el programa compilado, sin necesidad de recompilar el programa anfitrión). Estos lenguajes de script suelen ser imperativos u orientados a objetos.
\\

Introducir un lenguaje de scripting embebido en un videojuego tiene diversas aplicaciones. Algunas de ellas:
\begin{itemize}
	\item Aplicar toma de decisiones, ya sea para aplicar una IA o realizar cálculos en base al estado actual del mundo.
	\item Permitir el modding (permitir a terceros modificar o ampliar el contenido, guión o IA de un juego sin contar con acceso al código fuente).
	\item Introducir secuencias guionizadas, interactivas o no.
	\item Separar comportamientos y animaciones del motor del juego, tratando estos scripts a modo de datos.
\end{itemize}
En todos estos casos, se está separando del motor del juego cuestiones de lógica, con el fin de poder modificar comportamientos sin acceso al código fuente del motor del juego ni recompilar el proyecto.
\\

El objetivo es desarrollar un pequeño lenguaje de script interpretado, más sencillo de aprender, y con un paradigma ligeramente distinto basado en la siguiente idea: en un juego 2D, principalmente vemos sprites (imágenes desplazándose por la pantalla, cambiando su textura o imagen fuente para crear animaciones): son objetos con una imagen asignada, unas coordenadas, y quizá un escalado y rotación, que en cada fotograma van actualizándose.
\\

El lenguaje propuesto se basaría en parte en la idea de ECS (el patrón de desarrollo Entity Component System, que trataremos en el siguiente apartado de este documento), para tratar los objetos del juego como entidades. Estas entidades se ejecutan en paralelo de forma transparente para el programador.
\\

El objetivo es diseñar este lenguaje, y desarrollar un intérprete para ejecutarlo. La implementación de este intérprete se apoyaría en la librería SFML para gestionar gráficos, sonidos y entrada/salida, y comenzaría siendo una aplicación independiente.
\\

Pese a que inicialmente consideré diseñar un lenguaje compilado, la ventaja de un lenguaje de script es poder probar y modificar con rapidez, con lo que un lenguaje interpretado es óptimo para este fin.
\\

Las características deseadas en este lenguaje se resumen en:

\begin{itemize}
	\item Lenguaje de scripting, interpretado, para el apoyo al desarrollo de juegos 2D.
	\item Se pretende que el lenguaje propuesto sirva tanto como lenguaje de script embebido, como ejecutado en un intérprete independiente. Este intérprete, técnicamente, no dejaría de ser un juego “tonto” (con sus librerías para gráficos y sonido, gestión de entrada y salida, etc), totalmente controlado por scripts.
	\item Como lenguaje de scripting, abarca desde lógica sencilla hasta animaciones 2D y desarrollo de juegos.
	\item Ante todo es un lenguaje de scripting primero, y una herramienta de desarrollo de juegos después. La sencillez de uso y aprendizaje deben primar frente a todo lo demás.
	\item Debe ocultar la complejidad y las operaciones comunes de todo motor de juegos, incluso al usarse en modo standalone.
	\item Su sintaxis debe ser sencilla, pero en la medida de lo posible ser parecida a lenguajes como C++ o Java (en lo referente a operadores, bloques de código entre llaves, etc), para fomentar que portar código entre un hipotético juego en C++ y el lenguaje de scripting sea también lo más sencillo posible. Eso permite prototipar rápidamente funcionalidades en el lenguaje de script, y, de ser necesario, portarlo al programa anfitrión (escrito en C++), o extraer lógica del programa anfitrión a scripts.
	\item Se desea tener una abstracción del patrón ECS.
\end{itemize}


\section{Introducción a ECS: mundo de ejemplo} \label{ecsIntro}
El patrón Entity Component System está muy extendido en el desarrollo de juegos. Favorece composición frente a herencia (ahora entraremos en detalle). Un lenguaje de script simplificado que siga sus principios puede aportar un enfoque sencillo y versátil. Pero, ¿qué problema hay con la herencia?
\\

El paradigma orientación a objetos es muy intuitivo en el desarrollo de videojuegos: cada objeto del mundo puede representarse por una clase de modelo, y definimos una jerarquía para heredar características y comportamientos comunes. Pongamos un ejemplo: estamos desarrollando un juego en el que gestionamos una granja. Empezamos teniendo un gato, que puede maullar y jugar.
\\

Ampliamos nuestro juego, y ahora añadimos un perro. El perro también puede jugar por la granja y a diferencia del gato, ladra en lugar de maullar. Así que definimos una clase madre común con la acción que comparten, y así no repetir código.
\begin{center}
	\includegraphics[width=5cm]{archivos/ecs-01}
	\captionof{figure}{Diagrama UML de herencia para clases de animales}
	\label{ecs-01}
\end{center}

Los animales ensucian al jugar, así que añadimos a nuestro juego dos robots: uno que limpia cuando encuentra basura, y otro que castiga a los animales que ensucian cuando encuentran basura:
\begin{center}
	\includegraphics[width=5cm]{archivos/ecs-02}
	\captionof{figure}{Clases de robots}
	\label{ecs-02}
\end{center}

Durante un tiempo el juego está estable. Un día, un manager de la empresa nos pide añadir un perro robot castigador, que castiga a los gatos que ensucian ladrando. Es un robot que busca basura, pero a diferencia de los perros, no juega.
\\

¿Dónde cabe esta nueva clase en esta jerarquía? En un lenguaje como Java, con herencia simple, la solución puede no ser obvia, y requiere repensar toda la estructura de clases o bien duplicar métodos o sobrecargarlos para que no hagan nada. Por medio de la composición lo habríamos evitado.

\subsection{Composición frente a herencia}
En amarillo se marcan los componentes que definen comportamientos. Las distintas clases reutilizan los componentes que necesiten. Añadir más adelante un nuevo robot gato, que maúlle, limpie y castigue, sería trivial reutilizando los componentes que ya tenemos. Como veremos, en ECS todo esto se simplifica: los componentes serán simples contenedores de datos, y serán los sistemas los encargados de aplicar comportamientos, en base a estos componentes.
\begin{center}
	\includegraphics[width=14cm]{archivos/ecs-03}
	\captionof{figure}{Composición de comportamientos}
	\label{ecs-03}
\end{center}

\subsection{Qué es ECS: Componentes}
La idea básica de ECS es modelar nuestro mundo como entidades. Estas entidades son agregaciones de componentes. Los componentes no son más que pequeños contenedores de datos. Un par de ejemplos:
\begin{itemize}
	\item una posición (con variables X e Y en un sistema de coordenadas 2D)
	\item las características de un personaje en un juego de rol (nombre, atributos como fuerza, destreza, inteligencia, suerte, etc)
\end{itemize}
Los componentes sólo agrupan datos que aportan valor. En una implementación clásica, un componente es un struct de C, o una clase con algunos atributos públicos y ningún método que aplique lógica de negocio.
\\
Los componentes definen propiedades, y estas propiedades alterarán el comportamiento y características de las entidades.

\subsection{Qué es ECS: Entidades}
Decíamos que las entidades modelan los objetos del mundo. Una entidad puede ser un jugador, un enemigo, un proyectil, un robot de limpieza… cualquier cosa representable en nuestro juego.
\\

En una implementación típica, en realidad, se representa como un identificador único, es decir, un entero sin signo.
\\

Los componentes almacenarán un campo indicando el identificador de la entidad a la que pertenecen. Se evita el uso de punteros para estas referencias buscando simplicidad.
\\

Ya tenemos una representación de los objetos que hay en nuestro mundo, con sus propiedades. Podemos añadir comportamientos o propiedades a una entidad simplemente añadiéndole componentes (es decir, aplicando composición). Pero, ¿dónde va a estar implementada la lógica de negocio?

\subsection{Qué es ECS: Sistemas}
La tercera y última pieza son los sistemas. Los sistemas son clases o funciones que actúan sobre componentes o conjuntos de componentes, leen y modifican los valores de los componentes para aplicar la lógica de la aplicación. Ejemplos de sistemas:
\begin{itemize}
	\item Un sistema de movimiento, que calcula la nueva posición en cada fotograma de aquellas entidades que tienen una posición y una velocidad
	\item Un sistema de fuerzas, que modifica la velocidad de las entidades que cuentan con una posición, una aceleración, una masa (sobre la que aplicar gravedad), etc.
	\item Un sistema de detección de colisiones, para comprobar si el proyectil que ha lanzado el jugador ha impactado en algún enemigo u obstáculo.
\end{itemize}
Con estas tres piezas, añadir características al juego se limita a decidir qué componentes y sistemas necesito para ello.

\subsection{Qué es ECS: más allá de entidades, componentes y sistemas}
En la práctica, necesitaremos algunos mecanismos más para implementar el patrón ECS de forma que sea útil. Necesitamos mecanismos para instanciar e inicializar las entidades, así como algún modo para comunicar los sistemas entre sí, ya sea mediante un paso de eventos, o compartiendo información.
\\
El lenguaje descrito en los apartados siguientes pretende aportar un enfoque simplificado de ECS para facilitar las tareas que lleva a cabo un lenguaje de scripting en un juego, abstrayendo al usuario de algunos de los aspectos vistos hasta ahora.


\section{¿Qué entendemos por lenguaje de scripting en el contexto de los videojuegos?}
Los lenguajes de scripting son lenguajes que usamos para controlar otras aplicaciones. En videojuegos, su uso es más específico: se embeben en el juego para ejecutar guiones externos, es decir, código que puede ser modificado sin tener que recompilar el juego entero.
\\

Usar un lenguaje de scripting ofrece varias ventajas: aumentamos la productividad al no tener que esperar a recompilar innecesariamente el motor del juego por cambios en el guión del mismo, nos da una mayor modularidad al separar claramente el motor del juego de la lógica del mismo, y además puede permitir a los jugadores editar sus propios scripts sin necesidad de acceder al código fuente del juego, lo que facilita el modding.
\\

\section{Arquitectura básica de un videojuego}
A nivel de software, podemos definir una simplificación de un videojuego como una aplicación que representa un mundo lleno de elementos (actores, entidades u objetos, como queramos llamarlo) con un bucle que lee la entrada del usuario, actualiza el estado del mundo en consecuencia, y muestra el resultado en pantalla.
\\
Para ello, lo habitual es apoyarse en un framework o motor de juego que nos facilite la gestión de entrada y salida, de modo que sólo tengamos que centrarnos en la lógica:

\begin{center}
	\includegraphics[width=14cm]{archivos/arquitectura-1}
	\captionof{figure}{Arquitectura básica}
	\label{arquitectura-1}
\end{center}

Dentro de la lógica de la aplicación, es decir, la actualización de las entidades del mundo, intervienen diversos factores o subsistemas. Es aquí donde intervienen los intérpretes de scripts, entre otros:
\begin{center}
	\includegraphics[width=14cm]{archivos/arquitectura-2}
	\captionof{figure}{Arquitectura básica: detalle de actualización del mundo}
	\label{arquitectura-2}
\end{center}

Esta es una simplificación que oculta deliberadamente aspectos ciertos más complejos, pero irrelevantes en este punto, como son la gestión de estos subsistemas, o las máquinas de estados que gestionan las distintas partes del juego (ej: adecuar la lógica a si estamos en un menú, en una secuencia de vídeo, o en un nivel del juego, etc).
\\

Aunque cada aplicación tiene unas necesidades específicas, el intérprete de scripts habitualmente se encontraría en su propio estado (no es habitual que otros subsistemas lógicos actúen sobre los scripts, aunque pueden verse afectados por el contexto y la entrada).
\\

En el ámbito de este TFG, se plantea un prototipo de intérprete standalone que gestione la lógica del juego en base al script:
\begin{center}
	\includegraphics[width=14cm]{archivos/arquitectura-3}
	\captionof{figure}{Arquitectura básica: rol del intérprete}
	\label{arquitectura-3}
\end{center}
