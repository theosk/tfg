%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plantilla TFG/TFM
% Universidad de Murcia. Facultad de Informática
% Realizado por: José Manuel Requena Plens
% Modificado: Pablo José Rocamora Zamora
% Contacto: pablojoserocamora@gmail.com
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\chapter{Diseño y resolución del trabajo realizado}
\section{Visión de alto nivel del lenguaje}
\subsection{Descripción general}
Comencemos describiendo a muy alto nivel el lenguaje propuesto. Para ello, vamos a ver en primer lugar un ejemplo de script muy sencillo, y a continuación se expone a muy alto nivel en qué consiste cada una de sus partes.

\subsection{Ejemplo de programa}
Después de la introducción a ECS, es posible intuir a simple vista el comportamiento del script siguiente:
\begin{lstlisting}
program "Ejemplo";
component pos {
	float x;
	float y;
}

component speed {
	float x;
	float y;
}

system acceleration(pos, speed) {
	pos.x = pos.x + speed.x;
	pos.y = pos.x + speed.y;
}

entity player {
	component pos;
	component speed;
	system acceleration;
}

function main() {
	int playerId;
	playerId = new player;
	playerId.speed.x = 0.1;
	playerId.speed.y = 0.025;
	playerId.pos.x = 320;
	playerId.pos.y = 240;
}

\end{lstlisting}

\subsection{Construcciones facilitadas por el lenguaje}
En el apartado "Especificación del lenguaje" se expone en detalle la sintaxis del lenguaje. Se avanza una descripción de alto nivel de los principales elementos del lenguaje, que vemos que se acercan bastante a lo expuesto en la introducción a ECS del apartado \ref{ecsIntro}.

\subsubsection{Componentes}
Son contenedores de datos, que sirven como base para la composición de entidades. Un ejemplo de componente son unas coordenadas (x,y), o un vector de velocidad (magnitud, x, y). Los componentes contienen variables, pero no bloques de código.

\subsubsection{Entidades}
Son simples agregaciones de componentes, identificadas por un valor numérico único, de modo que podemos definir los objetos del mundo simplemente como un conjunto de componentes. De este modo, un personaje jugador podría ser una entidad con un componente gráfico, un componente de coordenadas, y un componente de atributos tales como su salud actual, salud máxima, puntos de magia, etc.
\\

Las entidades no contienen variables propias ni bloques de código, pero sí componentes. La inicialización de sus valores queda delegada a funciones que el usuario pueda definir para tal fin.


\subsubsection{Sistemas}
Son el elemento de lógica principal en ECS. Cada sistema tiene una instancia única, y posee sus propias variables y un bloque de código que se ejecuta en cada iteración para todas sus entidades (si el sistema está activo). Un sistema no actúa sobre todas las entidades del mundo, sino sólo sobre aquellas que se han suscrito al mismo.
\\

Los sistemas contienen una serie de variables propias, opcionales, y un bloque de código a ejecutar una vez por cada entidad suscrita en cada fotograma.
\\

Los sistemas tienen una única instancia, y sus variables son accesibles desde el exterior. Pueden usarse, pues, como mecanismo de coordinación entre sistemas, evitando así al implementador de scripts lidiar con la potencial complejidad de un paso de mensajes o captura de eventos.

\subsubsection{Funciones}
Realizan una serie de operaciones y tienen un valor de retorno, que puede ser de uno de los tipos básicos del lenguaje. Las funciones contienen una serie de variables propias, opcionales, así como una lista de argumentos que se le debe pasar al ser invocadas, y un bloque de código.
\\

Existe una función especial, "main", que debe definirse por parte del programador del script, y será la primera en ser llamada. En ella se espera que se inicialicen las entidades y se activen los sistemas iniciales.

\subsection{Flujo de ejecución de un programa}
El script comienza siempre con la ejecución de la función "main" del mismo. Si no está definida, el programa terminará informando del error. 
\\

Una vez ejecutada esta función, se ejecutará de manera indefinida los sistemas que hayan sido activados. Estos sistemas actúan sobre las entidades que estén suscritas a ellos, modificando los valores de sus componentes. Un sistema predefinido del lenguaje pinta en pantalla finalmente los gráficos, en base a las entidades que cuenten con el componente predefinido "sprite".
\\

Durante la ejecución de los sistemas, éstos pueden llamar a funciones, y tanto éstas como los propios sistemas pueden ejecutar instrucciones que creen o destruyan entidades, las suscriban a sistemas, o activen y desactiven sistemas.

\section{Especificación del lenguaje}
\subsection{Identificadores}
Los identificadores definen de manera única variables, funciones, componentes, sistemas y nombres de entidades. Están compuestos por una letra, seguido opcionalmente de cualquier combinación de caracteres alfanuméricos y guiones bajos.
\\

Son ejemplos identificadores válidos y distintos: jugador, Jugador, jugador\_2, Sa1ud, x, pos\_y. Son ejemplos de identificadores no válidos: player-health, 2players.

\subsection{Palabras clave}
El lenguaje cuenta con una serie de palabras clave reservadas, que no deben ser utilizadas como identificadores. Estas son: component, delete, do, else, entity, float, function, if, int, new, program, return, string, system y while.

\subsection{Tipos de datos}
Los tipos de datos contemplados por el lenguaje son: cadenas de texto, números enteros, números flotantes y referencias a entidades.

\subsection {Componentes}
Se definen mediante la palabra clave "component" seguida de un identificador, y una colección de definición variables encerradas por llaves:
\begin{lstlisting}
component icon {
	float x;
	float y;
	string image;
}
\end{lstlisting}

\subsection {Entidades}
Se definen mediante la palabra clave "entity" seguida de un identificador, y una colección de componentes previamente definidos, entre llaves:
\begin{lstlisting}
entity player {
	component icon;
	component speed;
}
\end{lstlisting}

\subsection {Sistemas}
Se definen mediante la palabra clave "system" seguida de un identificador, un listado de componentes requeridos entre paréntesis, y un cuerpo de sistema, con una definición de variables opcional, y un código a ejecutar sobre las entidades suscritas en cada iteración:
\begin{lstlisting}
system movement(pos, speed) {
	float someVar = 0.1;
	pos.x = pos.x + speed.x;
	pos.y = pos.y + speed.y + someVar;
}
\end{lstlisting}

\subsection {Funciones}
Se definen mediante la palabra clave "function" seguida de un identificador, un listado opcional de argumentos requeridos entre paréntesis, y un cuerpo de función, con una definición de variables opcional, y un código a ejecutar al ser invocada:
\begin{lstlisting}
function createPlayer(x, y) {
	int a;
	a = new player;
	player.pos.x = x;
	player.pos.y = y;
}
\end{lstlisting}


\subsection{Literales}
Los literales se utilizan para dar valores concretos a variables, paso de argumentos u operaciones aritméticas. Pueden ser de tipo numérico o cadenas.
\begin{itemize}
	\item Cadenas: se definen como cualquier combinación de caracteres delimitada por comillas dobles.
	\item Enteros: Uno o más dígitos, con un signo opcional. En caso de más de un dígito, el primero no puede ser cero.
	\item Flotantes: números decimales, separando la parte entera de la decimal por un punto.
\end{itemize}


\subsection{Variables}
Las variables se definen mediante una palabra clave para especificar su tipo (int, float, string) seguida de un identificador. Pueden ser definidas en funciones y sistemas, y su alcance está acotado en el contexto de los mismos).

\subsubsection{Definición e inicialización}
Las variables deben ser definidas al inicio del cuerpo de una función o sistema. Se les puede dar un valor por inicial:
int pos\_x;
float speed = 0.10;
\\

Un caso especial son los argumentos pasados al invocar una función, que a nivel interno quedan definidas como variables de la función con el valor que se indica al invocarla:

\begin{lstlisting}
program "Ejemplo";

function createPlayer(x, y) {
	int a;
	a = new player;
	a.pos.x = x; // 320
	a.pos.y = y; // 200
}

function main() {
	createPlayer(320, 200);
}
\end{lstlisting}

\subsubsection{Acceso y alcance}
Las funciones y sistemas pueden acceder directamente a sus propias variables. Las referencias a entidades, a su vez, proporcionan acceso a los valores de sus componentes:
\begin{lstlisting}
program "Ejemplo";

function createPlayer(x, y) {
	int a;
	a = new player;
	a.pos.x = x; // Acceso a una variable de un componente
	a.pos.y = y; // Acceso a una variable de un componente
}

function main() {
	createPlayer(320, 200);
}
\end{lstlisting}

De este modo, los sistemas y funciones pueden manipular a cualquier entidad cuya referencia tengan disponible. Es útil para inicializar entidades en funciones, o para que los sistemas puedan manipularlas, por ejemplo, un sistema de movimiento podría manipular las coordenadas de las entidades en cada tick.
\\

Por su parte, las variables de los sistemas son públicas a su vez, lo que permite a cualquier función o sistema acceder a sus valores:
\begin{lstlisting}
program "Ejemplo";

system playerControl {
	int playerId;
	...
}

function createPlayer() {
	playerControl.playerId = new player;
}
\end{lstlisting}

Esto es así para los sistemas, pero no para las funciones, cuyas variables son privadas.

\subsection{Operadores}
Se proporcionan los siguientes operadores:
\subsubsection{Operadores aritméticos}
Operan entre dos valores de una expresión, ya sean variables o literales. Son: +, -, *, /, \%.

\subsubsection{Comparadores}
Operan entre dos valores de una expresión, ya sean variables o literales, produciendo un resultado booleano. Son: >, <, ==, <=, >=, !=.

\subsubsection{Operadores unarios}
Se proporcionan operadores de cambio de signo y negación booleana; respectivamente: - y !.

\subsection{Estructuras de control}
\subsubsection{if/else}
Estructura if..else tradicional para el control de flujo, con la sintaxis:
\begin{lstlisting}
if (expresion) {
	...
} else {
	...
}
\end{lstlisting}

El bloque else es opcional.

\subsubsection{while}
Bucle condicionado por una expresión booleana:
\begin{lstlisting}
while (expresion) {
	...
}
\end{lstlisting}

\subsection{Elementos predefinidos en el lenguaje}
\subsubsection{Componente Sprite}
Se proporciona un componente predefinido, "sprite", con variables x, y, z, image, center\_x, center\_y y opacity. Sirve para informar al sistema interno de dibujo de qué entidades van a pintarse y con qué imagen. Cuando se actualiza el valor z de un sprite, se reordena la cola de dibujo en base a este valor para antes del siguiente fotograma.

\subsubsection{Sistema de dibujo}
Un sistema interno se encarga de dibujar en pantalla todas aquellas entidades que posean el componente "sprite".

\subsubsection{Sistema de manejo de ratón}
Un sistema "mouse" predefinido se gestiona de manera interna, proporcionando unas variables mouse.left, mouse.right, mouse.x y mouse.y para que el programador pueda verificar qué botones están pulsados y las coordenadas del ratón.

\subsubsection{Funciones predefinidas: funciones matemáticas}
Se proporcionan algunas funciones matemáticas útiles:
\begin{itemize}
	\item \textbf{sin(float);}: calcula el seno del valor dado.
	\item \textbf{cos(float);}: calcula el coseno del valor dado.
	\item \textbf{tan(float);}: calcula la tangente del valor dado.
	\item \textbf{asin(float);}: calcula el arcoseno del valor dado.
	\item \textbf{acos(float);}: calcula el arcocoseno del valor dado.
	\item \textbf{atan(float);}: calcula la arcotangente del valor dado.
	\item \textbf{abs(float);}: devuelve el valor absoluto de un valor dado.
	\item \textbf{sqrt(float);}: devuelve la raíz cuadrada de un valor dado.
	\item \textbf{pow(float, float);}: devuelve el resultado de elevar el primer parámetro al segundo.
	\item \textbf{rand(int, int);}: devuelve un entero aleatorio entre los valores pasados como parámetros.
\end{itemize}

\subsubsection{Funciones predefinidas: funciones de sistema}
\begin{itemize}
	\item \textbf{exit(int);}: termina la ejecución con el valor de retorno especificado.
\end{itemize}

\subsection{Especificación formal}
Se incluye la especificación formal de la gramática lenguaje en notación BNF, a partir de las reglas generadas en el desarrollo del prototipo.
\begin{lstlisting}
 <program> ::= "comment" program
| program_head program_body

<program_head> ::= "program" literalstring ";"

<program_body> ::= "comment" program_body
	| component program_body
	| system program_body
	| entity program_body
	| function program_body
	| ""

<component> ::= "component" identifier "{" variables "}"

<entity> ::= "entity" identifier "{" entityBody "}"

<entityBody> ::= "component" identifier ";" entityBody
	| "system" identifier ";" entityBody
	| "comment" entityBody
	| ""

<system> ::= "system" identifier arguments "{" variables codeblock "}"

<function> ::= "function" identifier arguments "{" variables codeblock "}"

<arguments> ::= "(" argument_list ")"
	| "(" ")"

<argument_list> ::= identifier
	| identifier "," argument_list

<variables> ::= variable variables
	| ""


<variable> ::= type identifier ";"
	| type identifier "=" intnumber ";"
	| type identifier "=" floatnumber ";"
	| type identifier "=" literalstring ";"

<full_identifier> ::= identifier
	| identifier "." identifier
	| identifier "." identifier "." identifier

<type> ::= "float"
	| "string"
	| "int"

<codeblock> ::= statement codeblock
	| ifelse codeblock
	| whileBlock codeblock
	| doWhileBlock codeblock
	| ";" codeblock
	| ""

<ifelse> ::= "if" "(" exp ")" "{" codeblock "}"
	| "if" "(" exp ")" "{" codeblock "}" "else" "{" codeblock "}"

<whileBlock> ::= "while" "(" exp ")" "{" codeblock "}"

<doWhileBlock> ::= "do" "{" codeblock "}" "while" "(" exp ")" ";"

<statement> ::= full_identifier "=" exp ";"
	| functionCall ";"
	| entityInstance ";"
	| entityDelete ";"
	| "return" ";"
	| "return" exp ";"
	| "comment"

<exp> ::= intnumber
	| floatnumber
	| literalstring
	| full_identifier
	| operation
	| "-" exp
	| "!" exp
	| "(" exp ")"
	| functionCall
	| entityInstance

<operation> ::= exp "+" exp
	| exp "-" exp
	| exp "/" exp
	| exp "*" exp
	| exp ">" exp
	| exp "<" exp
	| exp ">=" exp
	| exp "<=" exp
	| exp "==" exp
	| exp "!=" exp
	| exp "&&" exp
	| exp "||" exp
	| exp "%" exp

<functionCall> ::= identifier function_call_arguments

<function_call_arguments> ::= "(" function_call_argument_list ")"
	| "(" ")"

<function_call_argument_list> ::= function_call_argument
	| function_call_argument "," function_call_argument_list

<function_call_argument> ::= intnumber
	| floatnumber
	| literalstring
	| identifier

<entityInstance> ::= "new" identifier

<entityDelete> ::= "delete" identifier

\end{lstlisting}

Los símbolos de la gramática referentes a identificadores, literales y comentarios están definidos por las siguientes expresiones regulares:
\begin{lstlisting}
<identifier> ::= [a-zA-Z][a-zA-Z_0-9]*
<intnumber> ::= [1-9][0-9]*
<floatnumber> ::= [0-9]*\.*[0-9]+
<literalstring> ::= \"([^\\\"]|\\.)*\"
<comment> ::= "//".*
\end{lstlisting}


\section{Implementación y herramientas}
\subsection{C++, Flex y Bison}
Para el desarrollo del prototipo, se ha elegido C++ como lenguaje de programación, y las herramientas Flex y Bison para el desarrollo de los analizadores léxico y sintáctico, siguiendo las directrices de la variante C++ del manual.

\subsubsection{Estructuras de datos del intérprete}
Durante el análisis sintáctico, las reglas de la gramática permiten identificar las definiciones de entidades, componentes, sistemas y funciones, que son almacenadas en una clase catálogo. Después, durante la ejecución del intérprete, estos catálogos se usan del siguiente modo:
\begin{itemize}
	\item \textbf{Catálogo de definiciones de entidades}: almacena la definición de las entidades, y permite recuperarlas para crear instancias de las mismas cuando el programa lo requiera.
	\item \textbf{Catálogo de componentes}: almacena la definición de los componentes con sus variables y valores por defecto, y permite instanciarlos para añadirlos a las entidades.
	\item \textbf{Catálogo de sistemas}: almacena las definiciones de sistemas. A diferencia del resto de elementos, éstos no serán instanciados, ya que hay una única copia de cada sistema en el programa y almacenan información de estado (sus variables locales)
	\item \textbf{Catálogo de funciones}: similar al de sistemas, pero en la invocación de una función se crea una instancia de la misma cuyo tiempo de vida se limita a la ejecución de la misma.
	\item \textbf{Catálogo de instancias de entidades}: almacena las entidades instanciadas, con un identificador numérico único asociado. Hay dos diferencias con el catálogo de definiciones: estas instancias contienen los componentes con la información de estado, y puede haber un número arbitrario de instancias de un tipo de entidad determinado en tiempo de ejecución.
\end{itemize}

A nivel de implementación, cada uno de los elementos se resume del siguiente modo:
\begin{itemize}
	\item Entidades: tienen un nombre, identificador numérico único y una colección de componentes, así como una coleccion de nombres de sistemas a los que están suscritas.
	\item Componentes: tienen un nombre y una serie de variables.
	\item Sistemas: tienen un nombre, un listado de componentes requeridos para suscribirse, variables locales opcionales y un bloque de código.
	\item Funciones: tienen un nombre, una lista de argumentos, una lista de variables locales, y un bloque de código.
\end{itemize}

\subsubsection{Árboles de Sintaxis Abstracta}
Los bloques de código se implementan mediante un árbol de sintaxis. Existe una clase de tipo nodo base, y clases que heredan de ella sus atributos y sobrecargan el método de ejecución. Almacenan un valor como resultado de esta ejecución.
\\

La evaluación de un bloque de código se limita a recorrer el árbol recursivamente ejecutando sus nodos:



\begin{lstlisting}
class AstNode {
	...
	virtual void run() {
		if(firstChild != nullptr) {
			firstChild->run();
		}
		if(secondChild != nullptr) {
			secondChild->run();
	}
	...
}
\end{lstlisting}

Este árbol se construye durante la fase de análisis sintáctico, en la que se instancian nodos del tipo correspondiente a la regla de la gramática, y se le asignan sus hijos para facilitar su posterior ejecución:
\begin{lstlisting}
statement:
	full\_identifier EQUALS exp SEMICOLON {
		$$ = new AstNodeAssign($1, $3);
	}
	| functionCall SEMICOLON {
		$$ = $1;
	}
	| entityInstance SEMICOLON {
		$$ = $1;
	}
	| entityDelete SEMICOLON {
		$$ = $1;
	}
		| COMMENT { $$ = nullptr; }
	;

\end{lstlisting}

En el ejemplo, los parámetros del constructor de AstNodeAssign, son a su vez nodos Ast de distintos tipos, que serán evaluados recursivamente a la hora de ejecutar el nodo de tipo asignación:
\begin{lstlisting}
class AstNodeAssign: public AstNode {
public:
	AstNodeAssign(AstNode *fc, AstNode *sc) {
		firstChild = fc;
		secondChild = sc;
	}

	...
	void run() {
		AstNode::run(); // Llamada al método recursivo del padre
	
		// Implementación específica en base al contexto actual
		if(ECSCatalog::instance().currentState == "function") {
			runFunctionAssign((AstNodeVariable *)firstChild);
		}
	
		if(ECSCatalog::instance().currentState == "system") {
			runSystemAssign((AstNodeVariable *)firstChild);
		}
	}
};

\end{lstlisting}

Los nodos del árbol de sintaxis pueden anidarse de forma arbitraria para representar las operaciones que necesitemos. El siguiente ejemplo resuelve una operación aritmética sencilla implicando valores literales y una variable: $ 3.14 + 100.0*offset\_x $
\begin{center}
	\includegraphics[width=10cm]{archivos/diagrama-ast}
	\captionof{figure}{Ejemplo de árbol de sintaxis para una operación sencilla}
	\label{ast-01}
\end{center}


\subsection{SFML}
Para la implementación del prototipo se ha elegido el framework multimedia SFML. El motivo para ello es su excelente documentación, sencillez, y que está desarrollado pensando en su uso en C++, a diferencia de alternativas perfectamente válidas como SDL o Allegro cuya implementación principal es en C.
\\

Este framework proporciona abstracción para el manejo de E/S, imagen y sonido.

\subsection{Prototipo desarrollado: repositorio de código}
El desarrollo ha dado como resultado la implementación de un prototipo siguiendo el procedimiento descrito en este capítulo, y basado en la especificación del lenguaje. El código fuente del mismo puede encontrarse en el repositorio: \url{https://bitbucket.org/theosk/tfg/}