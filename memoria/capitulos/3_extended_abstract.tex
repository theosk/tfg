%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plantilla TFG/TFM
% Universidad de Murcia. Facultad de Informática
% Realizado por: José Manuel Requena Plens
% Modificado: Pablo José Rocamora Zamora
% Contacto: pablojoserocamora@gmail.com
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\chapter*{Extended Abstract}
\section{Introduction}
The video game industry has experienced a massive financial growth in the last couple of decades, both in quality and quantity. Projects have become more and more ambitious in scope and budget, and development teams as well as development time keeps growing, not just regarding big companies, but also for smaller independent studios. With this growth, the tools available have become more refined, and we have a wider range of more varied and powerful software at our disposal.
\\

These tools include, among others, those targeted at design tasks, world building and, of course, tools and languages geared towards AI development, dialogs, scripted events and cinematic sequences. These languages are known as scripting languages, and are usually simple and interpreted languages, which are embedded inside the game or program and are meant to simplify the development process by separating these tasks from the core game source code, allowing the scripts to be modified and run without having to compile the whole game. This means we can easily update our animation sequences, scripted events or AI as if they were mere external assets, and also makes it possible to share these scripts between users with no access to the game code, making modding possible to whatever degree the scripting language is capable of for end users.
\\

On the other hand, outside the scope of these tools, and ignoring really specific solutions, there's always the need to implement the core game mechanics and game engine. In video game development, the ECS pattern (Entity Component System, which we'll describe in detail later) has been gaining traction. It allows the developer to model the game world favoring composition over inheritance. That's something that makes lots of sense in a video game: we define our game world in which every actor or object is an entity, and these entities are a bunch of components put together, which are nothing more than simple data structures. Examples of components include a speed vector, a mass, or a coordinate vector.
\\

The goal of this work is to define the specification and develop a prototype for an interpreted scripting language with a very specific and unexplored set of features as of today, such as providing ECS features in the base language, making it more suitable for multimedia tasks. Entities will be defined internally as component sets with a unique identifier, creating an abstraction so the script developer won't need to know how any of these mechanics are implemented.
\\

A common and valid approach is to rely on existing solutions such as LUA and the likes, with bindings available for most used languages such as C, C++ or Java. Those are full featured languages that let us map functions from the host language (we could expose some C functions to the scripting language allowing them to interact with each other). These languages are mostly object oriented or imperative.
\\

Embedding scripting languages in a video game provides many advantages. To name a few:
\begin{itemize}
	\item We can extract from the core game a decision taking process dependent on the game world state.
	\item It makes modding easier: anyone can expand or modify the game in a controlled environment without having access to the source code.
	\item We can add scripted sequences externally, both automatic and interactive.
	\item We can define behaviours and animations outside of the game core, making these scripts act like game assets.
\end{itemize}
These items have one thing in common: game logic is being taken out of source code, so it can be altered without having to recompile the whole project (or even having access to the source).
\\

The goal is to define an easy to learn scripting language, with a simple paradigm based on the ECS pattern, treating all game objects as entities, and then develop a prototype interpreter for this language.
\\

Even though a compiled approach was taken into consideration, the main advantage of a scripting language is being able to test and modify the script quickly, so an interpreter looks like the logical solution.
\\

The following list details the main desired features of this language:

\begin{itemize}
	\item Interpreted scripting language, meant to be a support tool in 2D game development.
	\item It should be possible to both embed the interpreter in an existing game and to use it as an independent application, being able to run any valid script by itself.
	\item As a scripting language, it covers from simple logic to 2D animations and even simple game development.
	\item It's a scripting language first and a game development tool second, so being simple and easy to learn must be the main focus.
	\item It must hide the complexities and common architecture issues commonly faced by a game developer.
	\item The syntax must be simple, but as much similar to languages such as C++ and Java as possible, so the code can be easier to port between the game engine and the scripts. This allows us to make quick game prototypes in this language, and then rewrite them in other languages without a huge effort, or maybe move parts between the scripts and the source code.
	\item A high-level abstraction of the ECS pattern is desired.
\end{itemize}


\section{The ECS Pattern}
The ECS pattern is rapidly gaining traction among game developers. It favors composition over inheritance (we'll talk about this later). A simplified scripting language following these principles can offer an easy and versatile approach. But, first of all, what's wrong with inheritance?
\\

Object oriented paradigm is very intuitive when dealing with game development: every object in the world can be modeled as a class, and then we can define a class hierarchy in order to inherit common features and behaviours. Let's make a simple example up: we are making a game in which we are managing a farm. We have a cat that can meow and play.
\\

We extend our game, and now we have a dog. The dog can also play around the farm, but unlike the cat, it can bark instead of meowing. So we define a common parent class that defines the common action they share, saving us from repeating code in both classes.
\\

Pets leave a mess as they play, so we add a couple of robots to our game: a cleaning robot, and another robot that punishes the pets that trash the ground.
\\

The game becomes stable for a while. But one day, a manager shows up and ask us for a punisher robot dog, that punishes dirty cats by barking at them. It looks for trash, but unlike dogs, it never plays.
\\

This new class doesn't fit in our class hierarchy. In a language like Java, with simple inheritance, the solution may not be obvious, and forces us to rethink the whole class architecture, or have duplicated methods, or overload them so they do nothing in some classes. This would have been avoided by using composition.

\subsection{Composition vs Inheritance}
Another approach would be to leave the behaviours outside the pet definitions, and make classes with each basic behaviour (meow, bark, play, punish, clean). A pet class would now have a name and a behaviour list, so we can now create any world entity by just adding behaviour classes or components to it.

\subsection{What is ECS: Components}
The basic idea of ECS is modeling our world as a collection of entities. These entities are component aggregations, and the components are nothing more than little data containers. Some examples include:
\begin{itemize}
	\item a position component (with x,y variables in a 2D coordinate system)
	\item the player attributes in a role playing game (name, strength, dexterity, etc)
\end{itemize}
In a common implementation, a component would be a C struct, or a class with no methods and just public attributes.
\\
Components define properties that will alter how entities behave and evolve.

\subsection{What is ECS: Entities}
An entity can represent any object in the world: a player, a bullet, a cleaning robot... anything.
\\

In a common implementation, it would just be a unique unsigned integer, and its components would store this value.
\\

The only missing piece is the game logic.
\subsection{What is ECS: Systems}
The last piece is the system. Systems are classes or functions that alter the components in order to perform the game logic. Some examples may be:
\begin{itemize}
	\item A movement system, that calculates a new position every frame for those entities with speed and position components.
	\item A force system, updating the speed of those entities with a position, acceleration and mass.
	\item A collision detection system that checks whether a bullet is hitting a target.
\end{itemize}
By putting these three pieces together, adding features to the game becomes a matter of deciding what components and systems are needed.

\subsection{What is ECS: beyond entities, components and systems}
In practice, we still have more work to do in order to make this work. We need the ability to instantiate entities, as well as some way to send messages between systems, be it with events or sharing variables. There's also the catalogs and live instances management.
\\

One of the main goals is to provide a language based on a simplified ECS approach, so all these extra details are hidden and we can focus on writing the entities, components and systems, as well as user defined functions.
\\

\section{An example program}
In order to illustrate the proposed language, let's describe a really simple script. Every program will start with a main function, otherwise, the script won't be executed. After the main function is run if no functions or entities exist, the program will end. In order to do something useful, we will need to define some entities. But these entities need components, so we will define them first:
\begin{lstlisting}
component speed {
	float x;
	float y;
}

component acceleration {
	float x;
	float y;
}
\end{lstlisting}

With these components, we can now define an entity with meaningful properties:
\begin{lstlisting}
entity movingObject {
	component speed;
	component acceleration;
	component sprite;
	system movement;
}
\end{lstlisting}

In order to do something with these components, we define a system that handles them. This is the movement system required by the movingObject entity:
\begin{lstlisting}
system movement(speed, acceleration, sprite) {
	sprite.x = sprite.x + speed.x;
	sprite.y = sprite.y + speed.y;
	
	acceleration.x = acceleration.x * 0.1;
	if (acceleration.y < 0.98) {
		acceleration.y = acceleration.y + 0.01;
	}
	
	if (sprite.x > 640) { speed.x = -20;}
	if (sprite.x < 0) { speed.x = 20;}
}
\end{lstlisting}

Since the language is focused on video games, a default component for 2d sprites is provided: the "sprite" component. An internal system handles the output: every entity with a sprite component will be drawn on screen, based on its coordinates and image path. We must finally write, at least, the main function for all this to work.
\begin{lstlisting}
function main() {
	int id;
	id = new movingObject;
	id.sprite.x = 320;
	id.sprite.y = 240;
	id.sprite.image = "demo.png";
}
\end{lstlisting}

We could have written a function to make instances faster:
\begin{lstlisting}
function createInstance(x, y) {
	int id;
	id = new movingObject;
	id.sprite.x = x;
	id.sprite.y = y;
	id.sprite.image = "demo.png";
	return id;
}

function main() {
	createInstance(320, 120);
	createInstance(320, 240);
	createInstance(320, 360);
}
\end{lstlisting}

This little script creates some moving objects on screen. It's not specially useful, but serves its purpose as a means to illustrate the proposed language.


\section{Goals}
The following objectives are meant to be achieved:
\begin{enumerate}
	\item Define the scripting language and its grammar
	\item Write the language documentation
	\item Prototype implementation
	\item Prototype evaluation: running sample scripts
	\item Prototype evaluation: detecting potential improvements and enhacements
\end{enumerate}


\subsection{Language specification}
During this phase, the syntax and grammar will be defined, and a first approach at defining the interpreter specifications will be performed. This step concludes with an initial documentation.

\subsection{Prototype implementation}
The next phase will be the implementation. Based on the generated documentation, a prototype will be developed based on it that must comply with the specifications. In order to do so, standard tools will be used in order to optimize development times, such as Flex and Bison for the lexer and grammar analysis, and the multimedia features will be handled by the SFML framework. This phase ends with a working prototype coded in C++, able to run full scripts.

\subsection{Prototype evaluation}
The last step will be evaluating the prototype. It will consist on reviewing the results, and trying to identify its shortcomings and potential improvements. This information may help developing future versions of the interpreter or the language definition.
