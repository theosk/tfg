# TFG
## Óscar Berrocal Fráhija
### Curso 2018-2019


## Arquitectura del intérprete
Usamos una estructura de árbol para interpretar el programa, así como otras
estructuras adicionales.
La clase driver es el intérprete, a ella se añade en tiempo de parsing las
definiciones y estructuras de datos necesarias para ejecutar el script.

Catálogos:
systemCatalog, entityCatalog, componentCatalog, functionCatalog

Entidades activas:
activeEntities (contiene instancias de entidades, con sus componentes correspondientes)
Cada entidad instanciada contiene copias de sus componentes.

Sistemas activos:
listado de sistemas activos (puede ser un vector de tipo std::string)
Los sistemas no pueden ser instanciados, por lo que, en su catálogo, contienen las variables y código.

Métodos:
int createEntityInstance(string entityName, vector parameters)
void deleteEntityInstance(int id);
void addEntityToSystem(string entityTypeName, string systemName)
void enableSystem(string systemName)
void disableSystem(string systemName)
void callFunction(string functionName, vector parameters)
