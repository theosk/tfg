#include "driver.h"
#include "parser.hpp"
#include <cmath>
#include <fstream>

driver::driver ()
{
  result = 0;
  window = nullptr;
}


int driver::parse (const std::string &f)
{

  ECSCatalog::instance().addPredefinedComponents();
  ECSCatalog::instance().addPredefinedFunctions();
  ECSCatalog::instance().addPredefinedSystems();

  file = f;
  scan_begin ();
  yy::parser parser (*this);
  int res = parser.parse ();
  scan_end ();

  return res;
}



void driver::addVariable(ScriptVariable variable) {
  if(currentVariables.find(variable.name) != currentVariables.end()) {
    throw std::runtime_error("Multiple definition of variable " + variable.name);
    exit(1);
  }
  currentVariables[variable.name] = variable;
}

void driver::runSystems() {
  for (auto it : ECSCatalog::instance().systemCatalog) {
    if (it.second.isActive()) {
      it.second.run();
      this->deleteDeadEntities();
    }
  }
  // Finalmente: sistemas predefinidos
  this->runSpriteSystem();
  this->runWindowSystem();
}

void driver::deleteDeadEntities() {
  // Eliminados las entidades marcadas para borrar
  while (ECSCatalog::instance().entitiesToDelete.size()>0) {
    int id = ECSCatalog::instance().entitiesToDelete.back();
    ECSCatalog::instance().entitiesToDelete.pop_back();
    ECSCatalog::instance().deleteEntity(id);
  }
}

void handleMouseInput(sf::Event &event, sf::RenderWindow * window) {
  if (event.type == sf::Event::MouseMoved) {
    sf::Vector2i pixelPos = sf::Mouse::getPosition(*window);
    sf::Vector2f worldPos = window->mapPixelToCoords(pixelPos);
    ECSCatalog::instance().systemCatalog["mouse"].setVariable("x", std::to_string(worldPos.x));
    ECSCatalog::instance().systemCatalog["mouse"].setVariable("y", std::to_string(worldPos.y));
  }
  if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
    ECSCatalog::instance().systemCatalog["mouse"].setVariable("left", "1");
  } else {
    ECSCatalog::instance().systemCatalog["mouse"].setVariable("left", "0");
  }
  if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
    ECSCatalog::instance().systemCatalog["mouse"].setVariable("right", "1");
  } else {
    ECSCatalog::instance().systemCatalog["mouse"].setVariable("right", "0");
  }
}

void handleKeyboardInput(sf::Event &event, sf::RenderWindow * window) {
  std::string value;

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("left", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::Right) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("right", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::Up) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("up", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::Down) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("down", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::X) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("x", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::Z) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("z", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::Return) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("enter", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::Space) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("space", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("backspace", value);

  value = sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) ? "1" : "0";
  ECSCatalog::instance().systemCatalog["key"].setVariable("esc", value);

}

void driver::runMain() {
  if(ECSCatalog::instance().functionCatalog.find("main") == ECSCatalog::instance().functionCatalog.end()) {
    throw std::runtime_error("Main function is not defined.");
    exit(1);
  }
  this->runWindowSystem();

  Function main = ECSCatalog::instance().functionCatalog["main"];
  main.run();
  this->deleteDeadEntities();
  
  // Create the main window

  sf::Clock clock;
  float lastTime = 0;
  int fpsCont = 0;
  float meanFpsSum =0.0;
  // Comienza el bucle principal
  while (window->isOpen()) {
        // Procesamos eventos de SFML
        sf::Event event;
        while (window->pollEvent(event)) {
          switch(event.type){
              case sf::Event::Closed :
                  window->close();
                  break;
              case sf::Event::Resized :
                  updateScreenSize();
                  break;
              default:
                  break;
          }
          handleKeyboardInput(event, window);
          handleMouseInput(event, window);
            
        }
        // Limpiamos la pantalla
        window->clear();

        // Ejecutamos todos los sistemas un tick
        this->runSystems();

        float currentTime = clock.restart().asSeconds();
        float fps = 1.f / (currentTime);
        lastTime = currentTime;
        meanFpsSum +=fps;
        if(fpsCont++ > 60) {
          std::cout << (meanFpsSum/fpsCont) << std::endl;
          fpsCont = 0;
          meanFpsSum = 0.0;
        }

        // Mostramos el fotograma en pantalla
        window->display();
    }
    AssetManager::instance().stopMusic();
}

void driver::runSpriteSystem() {
  float x, y, size, opacity, angle;
  std::string strValue;
  std::string prevTextureStr;
  sf::Texture texture;


  if (ECSCatalog::instance().isReorderRequired()) {
    ECSCatalog::instance().reorderSprites();
  }
  for (auto id : ECSCatalog::instance().entitiesToDraw) {
    Entity &entity = ECSCatalog::instance().entityInstances[id];
    strValue = entity.getComponentVariable("sprite", "image").value;
    if (strValue == "") continue;
    if(prevTextureStr != strValue) {
      texture = AssetManager::instance().getTexture(strValue);
      sf::Sprite sprite;
      sprite.setTexture(texture);
      currentSprite = sprite;
    }
    prevTextureStr = strValue;

    x = entity.getFloatVariable("sprite", "x");
    y = entity.getFloatVariable("sprite", "y");
    currentSprite.setPosition(x, y);

    x = entity.getFloatVariable("sprite", "center_x");
    y = entity.getFloatVariable("sprite", "center_y");
    currentSprite.setOrigin(x, y);
    
    size = entity.getFloatVariable("sprite", "size");
    currentSprite.setScale(size, size);

    angle = entity.getFloatVariable("sprite", "angle");
    currentSprite.setRotation(-angle);

    opacity = entity.getFloatVariable("sprite", "opacity");
    currentSprite.setColor(sf::Color(255,255,255,opacity*255));

    window->draw(currentSprite);
  }
}

void driver::runWindowSystem() {
  if (std::stoi(ECSCatalog::instance().systemCatalog["window"].getVariableValue("update")) == 1) {
    int width, height, fps;
    std::string title;
    System &windowSystem = ECSCatalog::instance().systemCatalog["window"];
    width = std::stoi(windowSystem.getVariableValue("width"));
    height = std::stoi(windowSystem.getVariableValue("height"));
    fps = std::stoi(windowSystem.getVariableValue("fps"));
    title = windowSystem.getVariableValue("title");

    if(window != nullptr) {
      window->close();
    }
    window = new sf::RenderWindow(sf::VideoMode(width, height), title);
    window->setSize(sf::Vector2u(width, height));
    window->setTitle(title);
    

    window->setFramerateLimit(fps);

    ECSCatalog::instance().systemCatalog["window"].setVariable("update", "0");
    
   }

}


void driver::updateScreenSize(){
    System &windowSystem = ECSCatalog::instance().systemCatalog["window"];
    float width = std::stof(windowSystem.getVariableValue("width"));
    float height = std::stof(windowSystem.getVariableValue("height"));

    sf::View view = window->getView();
    float new_w = window->getSize().x;
    float new_h = window->getSize().y;

    float rel_x = new_w/(width);
    float rel_y = new_h/(height);
    float scale_x = 1.0f;
    float scale_y = 1.0f;
    float offset_x = 0.0f;
    float offset_y = 0.0f;

    if (rel_x > rel_y) {
        scale_x = rel_y/rel_x;
        offset_x = 0.5f*(1.0f-scale_x);
    }
    if (rel_y > rel_x) {
        scale_y = rel_x/rel_y;
        offset_y = 0.5f*(1.0f-scale_y);
    }

    view.setViewport(sf::FloatRect(offset_x, offset_y, scale_x, scale_y));
    window->setView(view);
}