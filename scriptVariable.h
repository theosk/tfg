#ifndef SCRIPY_VARIABLE_H
#define SCRIPY_VARIABLE_H

#include "datatypes.h"
#include <string>

class ScriptVariable {
public:
  std::string name;
  std::string value;
  DataType type;

  static DataType getDataTypeFromString(std::string datatype) {
    if (datatype == "int") {
        return Integer;
    }
    if (datatype == "float") {
        return Float;
    }
    if (datatype == "string") {
        return String;
    }
    throw "Invalid data type: " + datatype;
  }
};

#endif
