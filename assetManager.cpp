#include "assetManager.h"

#include <fstream>
#include <iostream>

AssetManager::AssetManager() {
    currentSound = 0;
}

int AssetManager::playSound(std::string path) {
    // 1. load the sound if not available
    sf::SoundBuffer buffer = loadSound(path);

    // 2. Play the sound
    int index = currentSound;
    sounds[index].stop();
    sounds[index].setBuffer(buffer);;
    sounds[index].play();
    currentSound = (index+1)%MAX_SOUNDS;
    return index;
}

int AssetManager::stopSound(int index) {
    sounds[index].stop();
    return 1;
}

int AssetManager::playMusic(std::string path) {
    music.stop();
    if(std::ifstream("assets/"+path).is_open()) {
        music.openFromFile("assets/"+path);
        music.setLoop(true);
        music.play();
        return 1;
    }
    return 0;
}

int AssetManager::stopMusic() {
    int isPlaying = music.Playing;
    music.stop();
    return isPlaying;
}

sf::Texture & AssetManager::getTexture(std::string path) {
    sf::Texture texture;
    if(textureCatalog.find(path) == textureCatalog.end()) {
        if(std::ifstream("assets/"+path).is_open()) {
            texture.setSmooth(true);
            texture.loadFromFile("assets/"+path);
        }
        textureCatalog[path] = texture;
    }
    return textureCatalog[path];
}

sf::SoundBuffer & AssetManager::loadSound(std::string path) {
    if(soundCatalog.find(path) == soundCatalog.end()) {
        if(std::ifstream("assets/"+path).is_open()) {
            sf::SoundBuffer buffer;
            buffer.loadFromFile("assets/"+path);
            soundCatalog[path] = buffer;
        } else {
            std::cout << "assets/"<< path << " not found" << std::endl;
        } 
    }
    return soundCatalog[path];
}