#include <iostream>
#include "driver.h"

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

int main (int argc, char *argv[])
{

  driver drv;
  // Parseamos el programa, creando las estructuras de datos del interprete.
  if (drv.parse (argv[1])) {
    return 1;
  }

 

  // Llamada a la función MAIN del script
  drv.runMain();

  return 0;
}
