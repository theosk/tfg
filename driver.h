#ifndef DRIVER_HH
#define DRIVER_HH
#include <string>
#include <map>
#include <vector>
#include "parser.hpp"

#include "ecs/catalog.h"
#include "scriptVariable.h"
#include "ecs/component.h"
#include "ecs/entity.h"
#include "ecs/system.h"
#include "ecs/function.h"
#include "ast/AstNode.h"

// Le damos a Flex el prototipo de yylex que queremos
# define YY_DECL \
  yy::parser::symbol_type yylex (driver& drv)

// ... y lo declaramos para el parser.
YY_DECL;

// Conductor del scanner y parse
class driver
{
public:
  driver ();
  std::map<std::string, int> variables;
  int result;
  // Ejecuta el parser en el fichero f. Devuelve 0 si va OK.
  int parse (const std::string& f);
  // El nombre del fichero a parsear.
  std::string file;

  // Métodos para encapsular la coorinación con Flex.
  // Manejo del scanner.
  void scan_begin ();
  void scan_end ();

  void addVariable(ScriptVariable variable);

  std::map<std::string, ScriptVariable> currentVariables;
  std::map<std::string, ScriptVariable> currentArguments;
  std::vector<std::string> currentParameters;

  // Ejecución del script
  void runSystems();
  void runMain();

  void deleteDeadEntities();

  // Localización del token usado por el escáner.
  yy::location location;

private:
  sf::Sprite currentSprite;

  void runSpriteSystem();
  void runWindowSystem();
  void updateScreenSize();
  sf::RenderWindow * window;
};
#endif // ! DRIVER_HH
