#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include <map>
#include <string>

const int MAX_SOUNDS = 64;

// Gráficos
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

class AssetManager {
    public:
        static AssetManager& instance(){
            static AssetManager instance;
            return instance;
        }

        int playSound(std::string path);
        sf::SoundBuffer & loadSound(std::string path);
        int stopSound(int index);
        int playMusic(std::string path);
        int stopMusic();
        sf::Texture & getTexture(std::string path);

    private:      
        std::map<std::string, sf::SoundBuffer> soundCatalog;
        sf::Sound sounds[MAX_SOUNDS];
        int currentSound = 0;
        sf::Music music;
        AssetManager();
        std::map<std::string, sf::Texture> textureCatalog;

};

#endif