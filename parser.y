
%skeleton "lalr1.cc" /* -*- C++ -*- */
%require "3.0"
%defines

%define api.token.constructor
%define api.value.type variant
%define parse.assert

%code requires {
  #include <string>
  class driver;
  #include "ecs/catalog.h"
  #include "ast/AstNode.h"
  #include "ast/AstNodeOperator.h"
  #include "ast/AstNodeAssign.h"
  #include "ast/AstNodeDoWhile.h"
  #include "ast/AstNodeFunction.h"
  #include "ast/AstNodeIfElse.h"
  #include "ast/AstNodeInstance.h"
  #include "ast/AstNodeInstanceDelete.h"
  #include "ast/AstNodeLiteral.h"
  #include "ast/AstNodeStatement.h"
  #include "ast/AstNodeWhile.h"
  #include "ast/AstNodeFor.h"
  #include "ast/AstNodeVariable.h"
  #include "ast/AstNodeReturn.h"
}

// The parsing context.
%param { driver& drv }
%locations
%define parse.trace
%define parse.error verbose

%code {
  # include "driver.h"
}

%define api.token.prefix {TOK_}
%token
  END  0  "end of file"
  EQUALS  "="
  MINUS   "-"
  PLUS    "+"
  STAR    "*"
  SLASH   "/"
  DOUBLE_MINUS  "--"
  DOUBLE_PLUS   "++"
  DOUBLE_STAR   "**"
  DOUBLE_SLASH  "//"
  PLUS_EQUALS "+="
  MINUS_EQUALS "-="
  SLASH_EQUALS "/="
  STAR_EQUALS "*="
  LPAREN  "("
  RPAREN  ")"
  GREATER_THAN ">"
  LESS_THAN "<"
  GREATER_EQUALS ">="
  LESS_EQUALS "<="
  DOUBLE_EQUALS "=="
  NOT_EQUALS "!="
  AND "&&"
  OR "||"
  NOT "!"
  MOD "%"
  DOT "."
  LBRACE  "{"
  RBRACE  "}"
  SEMICOLON   ";"
  COMMA   ","
  PROGRAM "program"
  ENTITY  "entity"
  COMPONENT "component"
  SYSTEM  "system"
  FUNCTION "function"
  RETURN "return"
  COMMENT "comment"
  T_STRING "string"
  T_INT "int"
  T_FLOAT "float"
  NEW "new"
  DELETE "delete"
  IF "if"
  ELSE "else"
  WHILE "while"
  DO "do"
  FOR "for"
;

%token <std::string> IDENTIFIER "identifier"
%token <std::string> STRING "literalstring"
%token <float> INTNUM "intnumber"
%token <float> FLOATNUM "floatnumber"
%type <AstNode *> codeblock
%type <AstNode *> full_statement
%type <AstNode *> statement
%type <AstNode *> exp
%type <AstNode *> operation
%type <AstNode *> full_identifier
%type <AstNode *> entityInstance
%type <AstNode *> entityDelete
%type <AstNode *> functionCall
%type <AstNode *> ifelse
%type <AstNode *> whileBlock
%type <AstNode *> doWhileBlock
%type <AstNode *> forBlock

/* %type <AstNode *> assignments
%type <AstNode *> assignment */
%type <std::string> type

%printer { yyo << $$; } <*>;

%%
%start program;
program:
  COMMENT program
  | program_head program_body {};

program_head:
  PROGRAM "literalstring" SEMICOLON {};

program_body:
  %empty
  | COMMENT program_body
  | component program_body
  | system program_body
  | entity program_body
  | function program_body
  ;

component:
  COMPONENT "identifier" LBRACE variables RBRACE {
    Component component;
    for (auto it : drv.currentVariables)
    {
      ScriptVariable variable = drv.currentVariables[it.first];
      component.setVariable(variable.name, variable.type, variable.value);
    }
    component.setName($2);
    ECSCatalog::instance().addComponentDefinition(component);
    drv.currentVariables.clear();
  };

entity:
  ENTITY "identifier" LBRACE entityBody RBRACE {
    Entity entityDefinition;
    entityDefinition.setName($2);
    for (auto it : drv.currentVariables)
    {
      if(it.second.value == "component") {
        Component component = ECSCatalog::instance().getComponentDefinition(it.second.name);
        entityDefinition.addComponent(component);
      } else if (it.second.value == "system") {
        entityDefinition.addSystem(it.second.name);
      }
    }
    ECSCatalog::instance().addEntityDefinition(entityDefinition);
    drv.currentVariables.clear();
  };

entityBody:
  COMPONENT "identifier" SEMICOLON entityBody {
    ScriptVariable var;
    var.name = $2;
    var.value = "component";
    drv.currentVariables[$2] = var;
  }
  |   
  SYSTEM "identifier" SEMICOLON entityBody {
    ScriptVariable var;
    var.name = $2;
    var.value = "system";
    drv.currentVariables[$2] = var;
  }
  | COMMENT entityBody {};
  | %empty {};


system:
  SYSTEM "identifier" arguments LBRACE variables codeblock RBRACE {
    System system;
    system.setName($2);
    for (auto it : drv.currentParameters)
    {
      system.addComponent(it);
    }

    for (auto it : drv.currentVariables)
    {
      ScriptVariable variable = drv.currentVariables[it.first];
      system.setVariable(variable.name, variable.type, variable.value);
    }
    if ($6 != nullptr) {
      system.setCode($6);
    } else {
      system.setCode(new AstNodeStatement(nullptr, nullptr));
    }

    system.enable();
    ECSCatalog::instance().addSystemDefinition(system);
    drv.currentVariables.clear();
    drv.currentParameters.clear();
  };

function:
  FUNCTION "identifier" arguments LBRACE variables codeblock RBRACE {
    Function functionDefinition;
    functionDefinition.setName($2);
    if ($6 != nullptr) {
      functionDefinition.setCode($6);
    } else {
      functionDefinition.setCode(new AstNodeStatement(nullptr, nullptr));
    }
    for (auto it : drv.currentParameters)
    {
      ScriptVariable variable;
      variable.name = it;
      functionDefinition.addArgument(variable);
    }
    for (auto it : drv.currentVariables)
    {
      ScriptVariable variable = drv.currentVariables[it.first];
      functionDefinition.setVariable(variable.name, variable.type, variable.value);
    }
    ECSCatalog::instance().addFunctionDefinition(functionDefinition);
    drv.currentVariables.clear();
    drv.currentParameters.clear();
    drv.currentArguments.clear();

  };

arguments:
  LPAREN argument_list RPAREN
  | LPAREN RPAREN {};

argument_list:
  "identifier" {
    drv.currentParameters.push_back($1);
  };
  | "identifier" COMMA argument_list {
    drv.currentParameters.push_back($1);
  };

variables:
  %empty
  | variable variables {}

variable:
  type "identifier" SEMICOLON {
      ScriptVariable variable;
      variable.name = $2;
      variable.type = ScriptVariable::getDataTypeFromString($1);
      variable.value = "";
      drv.addVariable(variable);
    };
    | type "identifier" EQUALS "intnumber" SEMICOLON {
        ScriptVariable variable;
        variable.name = $2;
        variable.type = ScriptVariable::getDataTypeFromString($1);
        variable.value = std::to_string($4);
        drv.addVariable(variable);
      };
    | type "identifier" EQUALS "floatnumber" SEMICOLON {
        ScriptVariable variable;
        variable.name = $2;
        variable.type = ScriptVariable::getDataTypeFromString($1);
        variable.value = std::to_string($4);
        drv.addVariable(variable);
      };
    | type "identifier" EQUALS "literalstring" SEMICOLON {
        ScriptVariable variable;
        variable.name = $2;
        variable.type = ScriptVariable::getDataTypeFromString($1);
        variable.value = $4.substr(1, $4.size() - 2);
        drv.addVariable(variable);
      };

full_identifier:
  "identifier" {
    $$ = new AstNodeVariable();
    $$->identifiers[0] = $1;
    $$->identifiers[1] = "";
    $$->identifiers[2] = "";
  };
  | "identifier" DOT "identifier" {
    $$ = new AstNodeVariable();
    $$->identifiers[0] = $1;
    $$->identifiers[1] = $3;
    $$->identifiers[2] = "";
  };
  | "identifier" DOT "identifier" DOT "identifier" {
    $$ = new AstNodeVariable();
    $$->identifiers[0] = $1;
    $$->identifiers[1] = $3;
    $$->identifiers[2] = $5;
  };

type:
  T_FLOAT { $$ = "float"; }
  | T_STRING { $$ = "string"; }
  | T_INT { $$ = "int"; }
;

%right "=";
%left "&&" "||";
%left "<" ">" "<=" ">=" "==" "!=";
%left "+" "-" "%";
%left "*" "/";
%left "!";

%precedence NEG;

codeblock:
  statement codeblock {
    $$ = new AstNodeStatement($1, $2);
  };
  | ifelse codeblock {
    $$ = new AstNodeStatement($1, $2);
  };
  | whileBlock codeblock {
    $$ = new AstNodeStatement($1, $2);
  };
  | doWhileBlock codeblock {
    $$ = new AstNodeStatement($1, $2);
  };
  | forBlock codeblock {
    $$ = new AstNodeStatement($1, $2);
  };  
  | SEMICOLON codeblock {
    $$ = $2;
  };
  | %empty {
    $$ = nullptr;
  };

ifelse: 
  IF LPAREN exp RPAREN LBRACE codeblock RBRACE {
    AstNodeIfelse * node = new AstNodeIfelse($6);
    node->condition = $3;
    $$ = node;
  };
  | IF LPAREN exp RPAREN LBRACE codeblock RBRACE ELSE LBRACE codeblock RBRACE {
    AstNodeIfelse * node = new AstNodeIfelse($6, $10);
    node->condition = $3;
    $$ = node;
  };

whileBlock: 
  WHILE LPAREN exp RPAREN LBRACE codeblock RBRACE {
    AstNodeWhile * node = new AstNodeWhile($6);
    node->condition = $3;
    $$ = node;
  };

doWhileBlock: 
  DO LBRACE codeblock RBRACE WHILE LPAREN exp RPAREN SEMICOLON {
    AstNodeDoWhile * node = new AstNodeDoWhile($3);
    node->condition = $7;
    $$ = node;
  };

forBlock: 
  FOR LPAREN full_statement full_statement statement RPAREN LBRACE codeblock RBRACE {

    AstNodeStatement * node = new AstNodeStatement($8, $5);
    AstNodeFor * nodeFor = new AstNodeFor($3, node);
    nodeFor->condition = $4;

    $$ = nodeFor;
  };

full_statement: 
  statement SEMICOLON {
    $$ = $1;
  }
  | COMMENT { $$ = nullptr; }
  
;
statement:
  full_identifier EQUALS exp {
    $$ = new AstNodeAssign($1, $3);
  }
  | full_identifier MINUS_EQUALS exp {
    $$ = new AstNodeAssign($1, new AstNodeOperator($1, $3, "-"));
  }
  | full_identifier PLUS_EQUALS exp {
    $$ = new AstNodeAssign($1, new AstNodeOperator($1, $3, "+"));
  }
  | full_identifier SLASH_EQUALS exp {
    $$ = new AstNodeAssign($1, new AstNodeOperator($1, $3, "/"));
  }
  | full_identifier STAR_EQUALS exp {
    $$ = new AstNodeAssign($1, new AstNodeOperator($1, $3, "*"));
  }
  | exp {
    $$ = $1;
  }
  | functionCall {
    $$ = $1;
  }
  | entityInstance {
    $$ = $1;
  }
  | entityDelete {
    $$ = $1;
  }
  | RETURN {
    $$ = new AstNodeReturn();
  }
  | RETURN exp {
    $$ = new AstNodeReturn($2);
  }
  | COMMENT { $$ = nullptr; }
  ;


  exp:
  "intnumber" {
    $$ = new AstNodeLiteral();
    ScriptVariable var;
    var.type = DataType::Integer;
    var.name = "literalNumber";
    var.value = std::to_string($1);
    $$->value = var;
  }
  |
  "floatnumber" {
    $$ = new AstNodeLiteral();
    ScriptVariable var;
    var.type = DataType::Float;
    var.name = "literalNumber";
    var.value = std::to_string($1);
    $$->value = var;
  }
  | "literalstring" {
    $$ = new AstNodeLiteral();
    ScriptVariable var;
    var.type = DataType::String;
    var.name = "literalString";
    var.value = $1.substr(1, $1.size() - 2); // No guardamos las comillas
    $$->value = var;
  }
  | full_identifier {
    $$ = $1;
  }
  | operation { $$ = $1; }
  | MINUS exp %prec NEG {
    $$ = new AstNodeOperator(nullptr, $2, "-");
  }
  | NOT exp {
    ScriptVariable var;
    var.type = DataType::Integer;
    var.name = "literalNumber";
    var.value = "0";
    AstNodeLiteral * zero = new AstNodeLiteral();
    zero->value = var;
    $$ = new AstNodeOperator(zero, $2, "==");
  }  
  | LPAREN exp RPAREN {
    $$ = $2;
  }
  | functionCall {
    $$ = $1;
  }
  | entityInstance {
    $$ = $1;
  }
  ;

operation: 
  exp PLUS exp { $$ = new AstNodeOperator($1, $3, "+");}
  | exp MINUS exp { $$ = new AstNodeOperator($1, $3, "-");}
  | exp SLASH exp { $$ = new AstNodeOperator($1, $3, "/");}
  | exp STAR exp { $$ = new AstNodeOperator($1, $3, "*");}
  | exp DOUBLE_PLUS {
      ScriptVariable var;
      var.type = DataType::Integer;
      var.name = "literalNumber";
      var.value = "1";
      AstNodeLiteral * one = new AstNodeLiteral();
      one->value = var;
      $$ = new AstNodeAssign($1, new AstNodeOperator($1, one, "+"));
    }
  | exp DOUBLE_MINUS { 
      ScriptVariable var;
      var.type = DataType::Integer;
      var.name = "literalNumber";
      var.value = "1";
      AstNodeLiteral * one = new AstNodeLiteral();
      one->value = var;
      $$ = new AstNodeAssign($1, new AstNodeOperator($1, one, "-"));
    }
  | exp DOUBLE_SLASH {
      $$ = new AstNodeAssign($1, new AstNodeOperator($1, $1, "/"));
    }
  | exp DOUBLE_STAR { 
    $$ = new AstNodeAssign($1, new AstNodeOperator($1, $1, "*"));
  }
  | exp GREATER_THAN exp { $$ = new AstNodeOperator($1, $3, ">");}
  | exp LESS_THAN exp { $$ = new AstNodeOperator($1, $3, "<");}
  | exp GREATER_EQUALS exp { $$ = new AstNodeOperator($1, $3, ">=");}
  | exp LESS_EQUALS exp { $$ = new AstNodeOperator($1, $3, "<=");}
  | exp DOUBLE_EQUALS exp { $$ = new AstNodeOperator($1, $3, "==");}
  | exp NOT_EQUALS exp { $$ = new AstNodeOperator($1, $3, "!=");}
  | exp AND exp { $$ = new AstNodeOperator($1, $3, "&&");}
  | exp OR exp { $$ = new AstNodeOperator($1, $3, "||");}
  | exp MOD exp { $$ = new AstNodeOperator($1, $3, "%");}
  ;

functionCall:
  "identifier" function_call_arguments {
    AstNodeFunction * function = new AstNodeFunction;
    function->value.type = DataType::String;
    function->value.name = "functionCall";
    function->value.value = $1;
    for (auto it : drv.currentArguments)
    {
      ScriptVariable variable = drv.currentArguments[it.first];
      function->arguments.insert(function->arguments.begin(), variable);
    }
    drv.currentArguments.clear();
    
    $$ = function;
  }
  ;

function_call_arguments:
  LPAREN function_call_argument_list RPAREN
  | LPAREN RPAREN {};

function_call_argument_list:
  function_call_argument {}
  | function_call_argument COMMA function_call_argument_list {}
  ;

  function_call_argument:
  "intnumber" {
    ScriptVariable var;
    int index = drv.currentArguments.size()+1;
    var.name = "var"+std::to_string(index);
    var.value = std::to_string($1);
    drv.currentArguments[var.name] = var;
  }
  | "floatnumber" {
    ScriptVariable var;
    int index = drv.currentArguments.size()+1;
    var.name = "var"+std::to_string(index);
    var.value = std::to_string($1);
    drv.currentArguments[var.name] = var;
  }
  | "literalstring" {
    ScriptVariable var;
    int index = drv.currentArguments.size()+1;
    var.name = "var"+std::to_string(index);
    var.value = $1.substr(1, $1.size() - 2);
    drv.currentArguments[var.name] = var;
  }
  | "identifier" {
    ScriptVariable var;
    int index = drv.currentArguments.size()+1;
    var.name = "var"+std::to_string(index);
    var.value = $1;
    var.type = Variable;
    drv.currentArguments[var.name] = var;
  };

entityInstance: NEW "identifier" { $$ = new AstNodeInstance($2); };

entityDelete: DELETE "identifier" { $$ = new AstNodeInstanceDelete($2); };

%%

void yy::parser::error (const location_type& l, const std::string& m)
{
  std::cerr << l << ": " << m << '\n';
}
