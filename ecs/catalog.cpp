#include "catalog.h"
#include <algorithm>

void ECSCatalog::addPredefinedComponents()
{
  Component sprite;
  sprite.setName("sprite");
  sprite.setVariable("image", DataType::String, "test.png");
  sprite.setVariable("x", DataType::Float, "0.0");
  sprite.setVariable("y", DataType::Float, "0.0");
  sprite.setVariable("z", DataType::Float, "0.0");
  sprite.setVariable("center_x", DataType::Float, "0.0");
  sprite.setVariable("center_y", DataType::Float, "0.0");
  sprite.setVariable("size", DataType::Float, "1.0");
  sprite.setVariable("opacity", DataType::Float, "1.0");
  sprite.setVariable("angle", DataType::Float, "0.0");
  this->addComponentDefinition(sprite);
}

void ECSCatalog::addPredefinedFunctions()
{
  predefinedFunctionsCatalog["exit"] = new PredefinedFunctionExit();
  predefinedFunctionsCatalog["sin"] = new PredefinedFunctionSin();
  predefinedFunctionsCatalog["cos"] = new PredefinedFunctionCos();
  predefinedFunctionsCatalog["tan"] = new PredefinedFunctionTan();
  predefinedFunctionsCatalog["asin"] = new PredefinedFunctionAsin();
  predefinedFunctionsCatalog["acos"] = new PredefinedFunctionAcos();
  predefinedFunctionsCatalog["atan"] = new PredefinedFunctionAtan();
  predefinedFunctionsCatalog["abs"] = new PredefinedFunctionAbs();
  predefinedFunctionsCatalog["sqrt"] = new PredefinedFunctionSqrt();
  predefinedFunctionsCatalog["pow"] = new PredefinedFunctionPow();
  predefinedFunctionsCatalog["rand"] = new PredefinedFunctionRand();

  predefinedFunctionsCatalog["sound_play"] = new PredefinedFunctionSoundPlay();
  predefinedFunctionsCatalog["sound_stop"] = new PredefinedFunctionSoundStop();
  predefinedFunctionsCatalog["music_play"] = new PredefinedFunctionMusicPlay();
  predefinedFunctionsCatalog["music_stop"] = new PredefinedFunctionMusicStop();

  predefinedFunctionsCatalog["preload_sound"] = new PredefinedFunctionPreloadSound();
  predefinedFunctionsCatalog["preload_image"] = new PredefinedFunctionPreloadImage();
}

void ECSCatalog::addPredefinedSystems()
{
  System mouse;
  mouse.setName("mouse");
  mouse.setVariable("x", Float, "0.0");
  mouse.setVariable("y", Float, "0.0");
  mouse.setVariable("left", Integer, "0");
  mouse.setVariable("right", Integer, "0");
  this->addSystemDefinition(mouse);

  System keyboard;
  mouse.setName("key");
  mouse.setVariable("z", Integer, "0");
  mouse.setVariable("x", Integer, "0");
  mouse.setVariable("left", Integer, "0");
  mouse.setVariable("right", Integer, "0");
  mouse.setVariable("up", Integer, "0");
  mouse.setVariable("down", Integer, "0");
  this->addSystemDefinition(keyboard);

  System window;
  window.setName("window");
  window.setVariable("width", Integer, "640");
  window.setVariable("height", Integer, "480");
  window.setVariable("title", String, "oscript");
  window.setVariable("fps", Integer, "60");
  window.setVariable("update", Integer, "1");
  this->addSystemDefinition(window);

}

void ECSCatalog::addEntityDefinition(Entity entity)
{
  if(entityCatalog.find(entity.getName()) != entityCatalog.end()) {
    throw std::runtime_error("Entity already defined: " + entity.getName());
    exit(1);
  }
  entityCatalog[entity.getName()] = entity;
}

void ECSCatalog::addComponentDefinition(Component component)
{
  if(componentCatalog.find(component.getName()) != componentCatalog.end()) {
    throw std::runtime_error("Component already defined: " + component.getName());
    exit(1);
  }
  componentCatalog[component.getName()] = component;
}

Component ECSCatalog::getComponentDefinition(std::string name)
{
  if(componentCatalog.find(name) == componentCatalog.end()) {
    throw std::runtime_error("Component not defined: " + name);
    exit(1);
  }
  return componentCatalog[name];
}

void ECSCatalog::addSystemDefinition(System system)
{
  if(systemCatalog.find(system.getName()) != systemCatalog.end()) {
    throw std::runtime_error("System already defined: " + system.getName());
    exit(1);
  }
  systemCatalog[system.getName()] = system;
}

void ECSCatalog::addFunctionDefinition(Function function)
{
  if(functionCatalog.find(function.getName()) != functionCatalog.end()) {
    throw std::runtime_error("Function already defined: " + function.getName());
    exit(1);
  }
  functionCatalog[function.getName()] = function;
}


int ECSCatalog::addEntityInstance(std::string entityName) {
  if(entityCatalog.find(entityName) == entityCatalog.end()) {
    throw std::runtime_error("Entity type is not defined: " + entityName);
    exit(1);
  }
  _isReorderRequired = true;
  entityCount++;
  Entity entity = entityCatalog[entityName];
  entity.setId(entityCount);
  entityInstances[entityCount] = entity;
  if (entity.hasComponent("sprite")) {
    entitiesToDraw.push_back(entityCount);
  }
  return entityCount;
}

void ECSCatalog::deleteEntity(int id) {
  entityInstances.erase(id);
  for (auto &it : systemCatalog) {
    it.second.removeEntity(id);
  }
  _isReorderRequired = true;
  entitiesToDraw.erase(std::remove(entitiesToDraw.begin(), entitiesToDraw.end(), id), entitiesToDraw.end());
}

void ECSCatalog::setReturnedValue(ScriptVariable variable) {
  if (currentState == "function") {
    currentFunction->setReturnValue(variable);
  } else if (currentState == "system") {
    throw "Systems have no return value!";
  } 
}

void ECSCatalog::setReorderRequired(bool value) {
 _isReorderRequired = value;
}


struct less_than_key
{
    inline bool operator() (const int& id1, const int& id2)
    {
      Entity &fst = ECSCatalog::instance().entityInstances[id1];
      Entity &sec = ECSCatalog::instance().entityInstances[id2];
      float firstZ = std::stof(fst.getComponentVariable("sprite", "z").value);
      float secondZ = std::stof(sec.getComponentVariable("sprite", "z").value);
      if (firstZ == secondZ) { return id1 > id2; }
      return (firstZ > secondZ);
    }
};

void ECSCatalog::reorderSprites() {
  std::sort(entitiesToDraw.begin(), entitiesToDraw.end(), less_than_key());
}