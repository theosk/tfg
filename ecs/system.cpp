#include "system.h"
#include "../ast/AstNode.h"
#include <iostream>

void System::setName(std::string name) {
  this->name = name;
}

std::string System::getName() {
  return this->name;
}


void System::addComponent(std::string name) {
  components.push_back(name);
}


bool System::isActive() {
  return this->active;
}

void System::enable() {
  this->active = true;
}

void System::disable() {
  this->active = false;
}
void System::setCode(AstNode * code) {
  this->code = code;
}

void System::run() {
  std::string prevState = ECSCatalog::instance().currentState;
  System * prevSystem = ECSCatalog::instance().currentSystem;
  ECSCatalog::instance().currentSystem = this;
  ECSCatalog::instance().currentState = "system";
  if (code != nullptr && this->active) {
    for (auto it : entities) {
      currentEntity = (Entity *) &ECSCatalog::instance().entityInstances[it];
      code->run();
    }
  }
  ECSCatalog::instance().currentState = prevState;
  ECSCatalog::instance().currentSystem = prevSystem;
}

void System::addEntity(int entityId) {
  entities.insert(entityId);
}

void System::removeEntity(int entityId) {
  entities.erase(entityId);
}


void System::setVariable(std::string name, DataType _type, std::string value)
{
    ScriptVariable variable;
    variable.name = name;
    variable.type = _type;
    if(_type != String && value == "") {value = "0";}
    variable.value = value;
    
    variables[name] = variable;
}

void System::setVariable(std::string name, std::string value) {
  variables[name].value = value;
}

ScriptVariable &System::getVariable(std::string name) {
  return variables[name];
}

std::string System::getVariableValue(std::string name) {
  if(name == "id" && currentEntity != nullptr) {
    return std::to_string(currentEntity->getId());
  }
  return variables[name].value;
}

bool System::hasVariable(std::string name) {
  return this->variables.find(name) != this->variables.end();
}


bool System::hasComponent(std::string name) {
  return std::count(components.begin(), components.end(), name);
}

void System::setCurrentEntityVariable(std::string componentName,std::string variableName, std::string value) {
  if(currentEntity != nullptr) {
    currentEntity->setComponentVariable(componentName, variableName, value);
  }
}


ScriptVariable & System::getCurrentEntityVariable(std::string componentName,std::string variableName) {
  ScriptVariable &entityVar = currentEntity->getComponentVariable(componentName, variableName);
  if(entityVar.value.empty()) {
    entityVar.value = "0";
  }
  return entityVar;
}

int System::getCurrentEntityId() {
  if(currentEntity != nullptr) {
    return currentEntity->getId();
  }
  return 0;
}