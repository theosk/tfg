#ifndef FUNCTION_H
#define FUNCTION_H

#include <string>
#include <vector>
#include <map>
#include "../scriptVariable.h"

class AstNode; // Avoid circular dependency

class Function
{
public:
  void setName(std::string name) { this->name = name; };
  std::string getName() { return this->name; };

  void setVariable(std::string name, DataType _type, std::string value);
  void setVariable(std::string name, std::string value);
  ScriptVariable &getVariable(std::string name);
  std::string getVariableValue(std::string name);
  bool hasVariable(std::string name);
  void addArgument(ScriptVariable argument);

  ScriptVariable run();
  void setCode(AstNode * code);
  void initArgumentVariables( std::vector <ScriptVariable> values);
  void setReturnValue(ScriptVariable variable) {
    if (!_isValueReturned) {
      _isValueReturned = true;
      returnValue = variable;
    }
  }

  ScriptVariable getReturnValue() {
    return returnValue;
  }

  bool isValueReturned() { return _isValueReturned; }

private:
  int id;
  std::string name;
  std::vector <ScriptVariable> arguments;
  std::map<std::string, ScriptVariable> variables;
  AstNode * code;
  ScriptVariable returnValue;
  bool _isValueReturned = false;
};

#endif
