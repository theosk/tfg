#include "component.h"

void Component::setVariable(std::string name, DataType _type, std::string value)
{
    ScriptVariable variable;
    variable.name = name;
    variable.type = _type;
    variable.value = value;
    variables[name] = variable;
}

void Component::setVariable(std::string name, std::string value) {
  variables[name].value = value;
}

ScriptVariable &Component::getVariable(std::string name) {
  return variables[name];
}

std::string Component::getVariableValue(std::string name) {
  return variables[name].value;
}

void Component::setName(std::string name)
{
    this->name = name;
}

std::string Component::getName()
{
  return name;
}
