#ifndef PREDEFINED_FUNCTIONS_H
#define PREDEFINED_FUNCTIONS_H

#include <string>
#include <map>
#include <vector>
#include "../scriptVariable.h"
#include "../assetManager.h"

#include <cmath>
#include <random>

#include <iostream>

class PredefinedFunction {
    public:
        virtual ScriptVariable run(std::vector <ScriptVariable> values){ return ScriptVariable(); };
        PredefinedFunction() {};
    protected:
        int argumentCount;
        void checkArgumentCount(std::vector <ScriptVariable> values) {
            if(values.size() != argumentCount) {
                throw std::runtime_error(std::string("Wrong argument count on function call: expcting ") 
                    + std::to_string(argumentCount) 
                    + std::string(", got ")
                    + std::to_string(values.size())
                );
            }
        }
};


class PredefinedFunctionExit: public PredefinedFunction {
    public:
        PredefinedFunctionExit() {
            argumentCount = 0;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            exit(0);
        }
};

class PredefinedFunctionSin: public PredefinedFunction {
    public:
        PredefinedFunctionSin() {
            argumentCount = 1;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(sin(std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionCos: public PredefinedFunction {
    public:
        PredefinedFunctionCos() {
            argumentCount = 1;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(cos(std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionAbs: public PredefinedFunction {
    public:
        PredefinedFunctionAbs() {
            argumentCount = 1;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(abs(std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionAcos: public PredefinedFunction {
    public:
        PredefinedFunctionAcos() {
            argumentCount = 1;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(acos(std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionAsin: public PredefinedFunction {
    public:
        PredefinedFunctionAsin() {
            argumentCount = 1;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(asin(std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionAtan: public PredefinedFunction {
    public:
        PredefinedFunctionAtan() {
            argumentCount = 1;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(atan(std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionSqrt: public PredefinedFunction {
    public:
        PredefinedFunctionSqrt() {
            argumentCount = 1;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(sqrt(std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionTan: public PredefinedFunction {
    public:
        PredefinedFunctionTan() {
            argumentCount = 1;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(tan(std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionPow: public PredefinedFunction {
    public:
        PredefinedFunctionPow() {
            argumentCount = 2;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            result.value = std::to_string(pow(std::stof(values[1].value), std::stof(values[0].value)));
            return result;
        }
};

class PredefinedFunctionRand: public PredefinedFunction {
    public:
        PredefinedFunctionRand() {
            argumentCount = 2;        
        };
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            std::random_device rd;
            std::mt19937 mt(rd());
            std::uniform_int_distribution<int> dist(std::stoi(values[1].value), 1+std::stoi(values[0].value));
            int value = dist(mt);
            result.value = std::to_string(value);
            return result;
        }
};


class PredefinedFunctionSoundPlay: public PredefinedFunction {
    public:
        PredefinedFunctionSoundPlay() {
            argumentCount = 1;        
        }
        
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            int value = -1;
            std::string path = values[0].value;
            value = AssetManager::instance().playSound(path);
            result.value = std::to_string(value);
            return result;
        }
};

class PredefinedFunctionSoundStop: public PredefinedFunction {
    public:
        PredefinedFunctionSoundStop() {
            argumentCount = 1;        
        }

        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            int index = std::stoi(values[0].value);
            int returnValue = AssetManager::instance().stopSound(index);
            
            ScriptVariable result;
            result.value = std::to_string(returnValue);
            return result;
        }
};



class PredefinedFunctionMusicPlay: public PredefinedFunction {
    public:
        PredefinedFunctionMusicPlay() {
            argumentCount = 1;        
        }
        
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            int value = -1;
            std::string path = values[0].value;
            value = AssetManager::instance().playMusic(path);
            result.value = std::to_string(value);
            return result;
        }
};

class PredefinedFunctionMusicStop: public PredefinedFunction {
    public:
        PredefinedFunctionMusicStop() {
            argumentCount = 0;
        }

        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            int index = std::stoi(values[0].value);
            int returnValue = AssetManager::instance().stopMusic();
            
            ScriptVariable result;
            result.value = std::to_string(returnValue);
            return result;
        }
};

class PredefinedFunctionPreloadSound: public PredefinedFunction {
    public:
        PredefinedFunctionPreloadSound() {
            argumentCount = 1;        
        }
        
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            int value = 1;
            std::string path = values[0].value;
            AssetManager::instance().loadSound(path);
            return result;
        }
};

class PredefinedFunctionPreloadImage: public PredefinedFunction {
    public:
        PredefinedFunctionPreloadImage() {
            argumentCount = 1;        
        }
        
        ScriptVariable run(std::vector <ScriptVariable> values) {
            checkArgumentCount(values);
            ScriptVariable result;
            int value = 1;
            std::string path = values[0].value;
            AssetManager::instance().getTexture(path);
            return result;
        }
};


#endif
