#ifndef ENTITY_H
#define ENTITY_H
#include "../scriptVariable.h"
#include "../datatypes.h"
#include "component.h"
#include <map>
#include <vector>
class Entity
{
public:
  void setId(int id) { this->id = id; };
  int getId() { return this->id; };

  void setName(std::string name) { this->name = name; };
  std::string getName() { return this->name; };  

  void addComponent(Component component); 
  bool hasComponent(std::string name);

  void addSystem(std::string name); 
  bool hasSystem(std::string name);

  ScriptVariable &getComponentVariable(std::string component, std::string variable);
  void setComponentVariable(std::string component, std::string variable, std::string value);
  float getFloatVariable(std::string component, std::string variable);

private:
  int id;
  std::string name;
  std::map<std::string, Component> components;
  std::vector<std::string> systems;

};

#endif