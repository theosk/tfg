#include "function.h"
#include "../ast/AstNode.h"
#include <iostream>
using namespace std;

ScriptVariable Function::run() {
    ScriptVariable result;

    std::string prevState = ECSCatalog::instance().currentState;
    Function * prevFunction = ECSCatalog::instance().currentFunction;
    ECSCatalog::instance().currentFunction = this;
    ECSCatalog::instance().currentState = "function";
    if (code != nullptr) {
      code->run();
      result = this->returnValue;
    }

    ECSCatalog::instance().currentState = prevState;
    ECSCatalog::instance().currentFunction = prevFunction;

    return result;
}

void Function::setCode(AstNode * code) {
  this->code = code;
}

void Function::setVariable(std::string name, DataType _type, std::string value)
{
    ScriptVariable variable;
    variable.name = name;
    variable.type = _type;
    variable.value = value;
    variables[name] = variable;
}

void Function::setVariable(std::string name, std::string value) {
  variables[name].value = value;
}

ScriptVariable &Function::getVariable(std::string name) {
  if(variables.find(name) != variables.end())
    return variables[name];
  throw "Variable not found";
}

std::string Function::getVariableValue(std::string name) {
  if(variables.find(name) != variables.end())
    return variables[name].value;
  throw "Variable not found";
}

bool Function::hasVariable(std::string name) {
  return this->variables.find(name) != this->variables.end();
}

void Function::addArgument(ScriptVariable argument) {
  this->arguments.push_back(argument);
}

void Function::initArgumentVariables(std::vector <ScriptVariable> values) {
  for (int i=0; i<values.size(); i++) {
      ScriptVariable currentVar = arguments[i];
      currentVar.value = values[i].value;
      if(values[i].type == Variable) {
        if(ECSCatalog::instance().currentState == "system") {
          System * currentSystem = ECSCatalog::instance().currentSystem;
          currentVar.value = currentSystem->getVariableValue(currentVar.value);

        } else {
          Function * currentFunc = ECSCatalog::instance().currentFunction;
          currentVar.value = currentFunc->getVariableValue(currentVar.value);
        }
      }
      variables[currentVar.name] = currentVar;
  }
  
}
