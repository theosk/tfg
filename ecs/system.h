#ifndef SYSTEM_H
#define SYSTEM_H
#include "entity.h"
#include "component.h"
#include <vector>
#include <set>
class AstNode;
class System
{
public:
  void setName(std::string name);
  std::string getName();
  void addComponent(std::string name);

  void setVariable(std::string name, DataType _type, std::string value);
  void setVariable(std::string name, std::string value);
  ScriptVariable &getVariable(std::string name);
  std::string getVariableValue(std::string name);
  bool hasVariable(std::string name);
  bool hasComponent(std::string name);

  bool isActive();
  void enable();
  void disable();
  void setCode(AstNode * code);
  void run();
  void addEntity(int entityId);
  void removeEntity(int entityId);
  int getCurrentEntityId();

  void setCurrentEntityVariable(std::string componentName,std::string variableName, std::string value);
  ScriptVariable & getCurrentEntityVariable(std::string componentName,std::string variableName);

protected:
  std::string name;
  std::vector<std::string> components;
  std::set<int> entities;
  bool active;
  AstNode * code;
  std::map<std::string, ScriptVariable> variables;
  Entity * currentEntity;

};

#endif
