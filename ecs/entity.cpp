#include "entity.h"
#include <iostream>
#include <algorithm>

void Entity::addComponent(Component component) {
    this->components[component.getName()] = component;
}

bool Entity::hasComponent(std::string name) {
    return this->components.find(name) != this->components.end();
}

ScriptVariable &Entity::getComponentVariable(std::string component, std::string variable) {
    return this->components[component].getVariable(variable);
}

float Entity::getFloatVariable(std::string component, std::string variable) {
    if(this->hasComponent(component)) {
        return std::stof(this->components[component].getVariable(variable).value);
    }
    return 0.0;
}

void Entity::setComponentVariable(std::string component, std::string variable, std::string value) {
    if(this->hasComponent(component)) {
        this->components[component].setVariable(variable, value);
    }
}


void Entity::addSystem(std::string name) {
    this->systems.push_back(name);
}

bool Entity::hasSystem(std::string name) {
    return std::find(this->systems.begin(), this->systems.end(), name) != this->systems.end();
}
