#ifndef COMPONENT_H
#define COMPONENT_H
#include "../scriptVariable.h"
#include "../datatypes.h"
#include <map>
class Component
{
public:
  void setName(std::string name);
  std::string getName();
  void setVariable(std::string name, DataType _type, std::string value);
  void setVariable(std::string name, std::string value);
  ScriptVariable &getVariable(std::string name);
  std::string getVariableValue(std::string name);

private:
  std::string name;
  std::map<std::string, ScriptVariable> variables;

};

#endif
