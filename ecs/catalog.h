#ifndef CATALOG_H
#define CATALOG_H

#include "component.h"
#include "entity.h"
#include "system.h"
#include "function.h"
#include "predefinedFunctions.h"

class ECSCatalog {

public:
  static ECSCatalog& instance(){
    static ECSCatalog instance;
    return instance;
  }

  // Agregar componentes predefinidos por el lenguaje
  void addPredefinedComponents();
  void addPredefinedFunctions();
  void addPredefinedSystems();
  
  // Agregar definiciones a los catálogos
  void addComponentDefinition(Component component);
  void addEntityDefinition(Entity entity);
  void addSystemDefinition(System system);
  void addFunctionDefinition(Function function);

  Component getComponentDefinition(std::string name);

  int addEntityInstance(std::string entityName);
  void deleteEntity(int id);

  void setReorderRequired(bool value);
  void reorderSprites();
  bool isReorderRequired() {return _isReorderRequired; }

  // Definiciones de catálogos
  std::map<std::string, Component> componentCatalog;
  std::map<std::string, Entity> entityCatalog;
  std::map<std::string, System> systemCatalog;
  std::map<std::string, Function> functionCatalog;
  std::map<std::string, PredefinedFunction*> predefinedFunctionsCatalog;
  
  // Instancias de entidades
  int entityCount;
  std::map<int, Entity> entityInstances;
  std::vector<int> entitiesToDelete;
  std::vector<int> entitiesToDraw;

  // Referencias a la ejecución actual
  System * currentSystem;
  Function * currentFunction;
  std::string currentState;
  void setReturnedValue(ScriptVariable variable);

private:
  bool _isReorderRequired;
  ECSCatalog() {
    entityCount = 0;
    _isReorderRequired = true;
  };
  ~ECSCatalog() {};

};

#endif
