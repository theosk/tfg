%{ /* -*- C++ -*- */
# include <cerrno>
# include <climits>
# include <cstdlib>
# include <cstring> // strerror
# include <string>
# include "driver.h"
# include "parser.hpp"
%}

%option noyywrap nounput noinput batch debug caseless

%{
  // A number symbol corresponding to the value in S.
  yy::parser::symbol_type
  make_INTNUM (const std::string &s, const yy::parser::location_type& loc);

  yy::parser::symbol_type
  make_FLOATNUM (const std::string &s, const yy::parser::location_type& loc);
%}

id    [a-zA-Z][a-zA-Z_0-9]*
int   [1-9][0-9]*
float   [0-9]*\.*[0-9]+
blank [ \t\r]
literalstring \"([^\\\"]|\\.)*\"
comment "//".*

%x incl

%{
  // Code run each time a pattern is matched.
  # define YY_USER_ACTION  loc.columns (yyleng);

  #define MAX_INCLUDE_DEPTH 128
  YY_BUFFER_STATE include_stack[MAX_INCLUDE_DEPTH];
  int include_stack_ptr = 0;  
%}

%%

%{
  // A handy shortcut to the location held by the driver.
  yy::location& loc = drv.location;
  // Code run each time yylex is called.
  loc.step ();
%}
include             BEGIN(incl);

<incl>[ \t]*      /* eat the whitespace */
<incl>[^ \t\n]+   { /* got the include file name */
        if ( include_stack_ptr >= MAX_INCLUDE_DEPTH )
            {
            fprintf( stderr, "Includes nested too deeply" );
            exit( 1 );
            }

        include_stack[include_stack_ptr++] =
            YY_CURRENT_BUFFER;

        yyin = fopen( yytext, "r" );

        if ( ! yyin )
            throw "Can not include file";
        
        yy_switch_to_buffer(
            yy_create_buffer( yyin, YY_BUF_SIZE ) );

        BEGIN(INITIAL);
        }

{blank}+   loc.step ();
\n+        loc.lines (yyleng); loc.step ();




"-"        return yy::parser::make_MINUS  (loc);
"+"        return yy::parser::make_PLUS   (loc);
"*"        return yy::parser::make_STAR   (loc);
"/"        return yy::parser::make_SLASH  (loc);
"--"        return yy::parser::make_DOUBLE_MINUS  (loc);
"++"        return yy::parser::make_DOUBLE_PLUS   (loc);
"**"        return yy::parser::make_DOUBLE_STAR   (loc);
"//"        return yy::parser::make_DOUBLE_SLASH  (loc);
"-="        return yy::parser::make_MINUS_EQUALS  (loc);
"+="        return yy::parser::make_PLUS_EQUALS   (loc);
"*="        return yy::parser::make_STAR_EQUALS   (loc);
"/="        return yy::parser::make_SLASH_EQUALS  (loc);
"("        return yy::parser::make_LPAREN (loc);
")"        return yy::parser::make_RPAREN (loc);
"="        return yy::parser::make_EQUALS (loc);
">"        return yy::parser::make_GREATER_THAN (loc);
"<"        return yy::parser::make_LESS_THAN (loc);
">="        return yy::parser::make_GREATER_EQUALS (loc);
"<="        return yy::parser::make_LESS_EQUALS (loc);
"=="        return yy::parser::make_DOUBLE_EQUALS (loc);
"!="        return yy::parser::make_NOT_EQUALS (loc);
"&&"        return yy::parser::make_AND (loc);
"||"        return yy::parser::make_OR (loc);
"!"        return yy::parser::make_NOT (loc);
"%"        return yy::parser::make_MOD (loc);
"program"  return yy::parser::make_PROGRAM (loc);
"component"  return yy::parser::make_COMPONENT (loc);
"system"   return yy::parser::make_SYSTEM (loc);
"entity"   return yy::parser::make_ENTITY (loc);
"function" return yy::parser::make_FUNCTION (loc);
"return"   return yy::parser::make_RETURN (loc);
";"        return yy::parser::make_SEMICOLON (loc);
","        return yy::parser::make_COMMA (loc);
"{"        return yy::parser::make_LBRACE (loc);
"}"        return yy::parser::make_RBRACE (loc);
"."        return yy::parser::make_DOT (loc);
"string"   return yy::parser::make_T_STRING (loc);
"float"    return yy::parser::make_T_FLOAT (loc);
"int"      return yy::parser::make_T_INT (loc);
"new"      return yy::parser::make_NEW (loc);
"delete"   return yy::parser::make_DELETE (loc);
"if"       return yy::parser::make_IF (loc);
"else"     return yy::parser::make_ELSE (loc);
"while"    return yy::parser::make_WHILE (loc);
"for"      return yy::parser::make_FOR (loc);
"do"       return yy::parser::make_DO (loc);


{int}      return make_INTNUM (yytext, loc);
{float}    return make_FLOATNUM (yytext, loc);
{id}       return yy::parser::make_IDENTIFIER (yytext, loc);
{literalstring}   return yy::parser::make_STRING (yytext, loc);
{comment}  return yy::parser::make_COMMENT (loc);

.          {
             throw yy::parser::syntax_error
               (loc, "invalid character: " + std::string(yytext));
}

<<EOF>>   {

  if ( --include_stack_ptr >= 0 )
      {
      yy_delete_buffer( YY_CURRENT_BUFFER );
      yy_switch_to_buffer(
            include_stack[include_stack_ptr] );
      } else {
        return yy::parser::make_END (loc);
      }
} 
%%

yy::parser::symbol_type
make_INTNUM (const std::string &s, const yy::parser::location_type& loc)
{
  errno = 0;
  long n = strtol (s.c_str(), NULL, 10);
  if (! (INT_MIN <= n && n <= INT_MAX && errno != ERANGE))
    throw yy::parser::syntax_error (loc, "integer is out of range: " + s);
  return yy::parser::make_INTNUM ((int) n, loc);
}

yy::parser::symbol_type
make_FLOATNUM (const std::string &s, const yy::parser::location_type& loc)
{
  float n = strtof (s.c_str(), NULL);
  return yy::parser::make_FLOATNUM (n, loc);
}


void
driver::scan_begin ()
{
  yy_flex_debug = false;
  if (file.empty () || file == "-")
    yyin = stdin;
  else if (!(yyin = fopen (file.c_str (), "r")))
    {
      std::cerr << "cannot open " << file << ": " << strerror(errno) << '\n';
      exit (EXIT_FAILURE);
    }
}


void
driver::scan_end ()
{
  fclose (yyin);
}
