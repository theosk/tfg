
RUNTIME = runtime
LIBS=-lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system
STD_VER=-std=c++11
LIBPATH_LINUX=./libs/linux

AST_FILES=astnode.o astnodeoperator.o astnodeassign.o astnodedowhile.o astnodefunction.o astnodeifelse.o astnodeinstance.o astnodeinstancedelete.o \
	astnodeliteral.o astnodestatement.o astnodevariable.o astnodewhile.o astnodereturn.o astnodefor.o

OBJECT_FILES=scanner.o parser.o entity.o component.o system.o function.o catalog.o driver.o assetManager.o

all: $(AST_FILES) $(OBJECT_FILES) 
	g++ -O1 $(STD_VER) main.cpp $(AST_FILES) $(OBJECT_FILES) $(LIBS) -o $(RUNTIME)

static: $(AST_FILES) $(OBJECT_FILES) 
	g++ -O1 $(STD_VER) main.cpp $(AST_FILES) $(OBJECT_FILES) $(LIBS) -Wl,-rpath=$(LIBPATH_LINUX) -o $(RUNTIME)

all_profile: $(AST_FILES) $(OBJECT_FILES) 
	g++ -pg $(STD_VER) $(AST_FILES) main.cpp $(OBJECT_FILES)  $(LIBS) -o $(RUNTIME)

profile: all_profile
	valgrind --tool=callgrind ./$(RUNTIME) code_samples/demo.osk
	kcachegrind callgrind.out.*
	rm callgrind.out.*

astnode.o: ast/AstNode.h ast/AstNode.cpp
	g++ $(STD_VER) -c ast/AstNode.cpp  -o astnode.o

astnodeoperator.o: ast/AstNode.h ast/AstNodeOperator.h ast/AstNodeOperator.cpp
	g++ $(STD_VER) -c ast/AstNodeOperator.cpp  -o astnodeoperator.o

astnodeassign.o: ast/AstNode.h ast/AstNodeAssign.h ast/AstNodeAssign.cpp
	g++ $(STD_VER) -c ast/AstNodeAssign.cpp -o astnodeassign.o

astnodedowhile.o: ast/AstNode.h ast/AstNodeDoWhile.h ast/AstNodeDoWhile.cpp
	g++ $(STD_VER) -c ast/AstNodeDoWhile.cpp -o astnodedowhile.o

astnodefunction.o: ast/AstNode.h ast/AstNodeFunction.h ast/AstNodeFunction.cpp
	g++ $(STD_VER) -c ast/AstNodeFunction.cpp -o astnodefunction.o

astnodeifelse.o: ast/AstNode.h ast/AstNodeIfElse.h ast/AstNodeIfElse.cpp
	g++ $(STD_VER) -c ast/AstNodeIfElse.cpp -o astnodeifelse.o

astnodeinstance.o: ast/AstNode.h ast/AstNodeInstance.h ast/AstNodeInstance.cpp
	g++ $(STD_VER) -c ast/AstNodeInstance.cpp -o astnodeinstance.o

astnodeinstancedelete.o: ast/AstNode.h ast/AstNodeInstanceDelete.h ast/AstNodeInstanceDelete.cpp
	g++ $(STD_VER) -c ast/AstNodeInstanceDelete.cpp -o astnodeinstancedelete.o

astnodeliteral.o: ast/AstNode.h ast/AstNodeLiteral.h ast/AstNodeLiteral.cpp
	g++ $(STD_VER) -c ast/AstNodeLiteral.cpp -o astnodeliteral.o

astnodestatement.o: ast/AstNode.h ast/AstNodeStatement.h ast/AstNodeStatement.cpp
	g++ $(STD_VER) -c ast/AstNodeStatement.cpp -o astnodestatement.o

astnodevariable.o: ast/AstNode.h ast/AstNodeVariable.h ast/AstNodeVariable.cpp
	g++ $(STD_VER) -c ast/AstNodeVariable.cpp -o astnodevariable.o

astnodewhile.o: ast/AstNode.h ast/AstNodeWhile.h ast/AstNodeWhile.cpp
	g++ $(STD_VER) -c ast/AstNodeWhile.cpp -o astnodewhile.o

astnodefor.o: ast/AstNode.h ast/AstNodeFor.h ast/AstNodeFor.cpp
	g++ $(STD_VER) -c ast/AstNodeFor.cpp -o astnodefor.o

astnodereturn.o: ast/AstNode.h ast/AstNodeReturn.h ast/AstNodeReturn.cpp
	g++ $(STD_VER) -c ast/AstNodeReturn.cpp -o astnodereturn.o

scanner.o: parser.o scanner.l
	flex -o scanner.cpp scanner.l
	g++ $(STD_VER) -c scanner.cpp -o scanner.o

parser.o: ast/AstNode.h parser.y
	bison -vo parser.cpp parser.y
	g++ $(STD_VER) -c parser.cpp -o parser.o

component.o: ecs/component.cpp
	g++ $(STD_VER) -c ecs/component.cpp -o component.o

system.o: ecs/system.cpp ast/AstNode.h
	g++ $(STD_VER) -c ecs/system.cpp -o system.o

entity.o: ecs/entity.cpp
	g++ $(STD_VER) -c ecs/entity.cpp -o entity.o

function.o: ecs/function.cpp ast/AstNode.h
	g++ $(STD_VER) -c ecs/function.cpp -o function.o

catalog.o: ecs/catalog.cpp
	g++ $(STD_VER) -c ecs/catalog.cpp -o catalog.o

assetManager.o: assetManager.cpp assetManager.h
	g++ $(STD_VER) -c assetManager.cpp -o assetManager.o


driver.o: driver.cpp
	g++ $(STD_VER) -c driver.cpp -o driver.o
	
clean:
	rm -rf scanner.cpp
	rm -rf parser.cpp parser.hpp location.hh position.hh stack.hh
	rm *.o
	rm -rf $(RUNTIME)

demo: all
	./$(RUNTIME) code_samples/demo.osk
