#include "AstNodeAssign.h"

  AstNodeAssign::AstNodeAssign(AstNode *fc, AstNode *sc) {
    firstChild = fc;
    secondChild = sc;
  }

  void AstNodeAssign::runFunctionAssign(AstNodeVariable * varNode) {
    std::string rightValue = secondChild->returnValue.value !="" ? secondChild->returnValue.value : parseFunctionVariable(secondChild);

    // 1. simple identifier: is my variable
    if(varNode->numIdentifiers == 1) {
      ECSCatalog::instance().currentFunction->setVariable(varNode->identifiers[0], rightValue);
    }
    // 2. Double identifier: Must be a system.variable.
    // Ex: playerMovement.playerId = ...
    else if(varNode->numIdentifiers == 2) {
        System &system = ECSCatalog::instance().systemCatalog[varNode->identifiers[0]];
        if(system.hasVariable(varNode->identifiers[1])) {
          system.setVariable(varNode->identifiers[1], rightValue);
        }
    }
    // 3. Triple identifier: entity variable in function
    // Ex: a.speed.x = ...
    else if(varNode->numIdentifiers == 3) {
      // 3.1 Check if first part is any of my variables, so it's entity.variable
      if (ECSCatalog::instance().currentFunction->hasVariable(varNode->identifiers[0])) {
        std::string identifierStr = ECSCatalog::instance().currentFunction->getVariableValue(varNode->identifiers[0]);
        int id = std::stoi(identifierStr);
        Entity &entity = ECSCatalog::instance().entityInstances[id];
        entity.setComponentVariable(varNode->identifiers[1], varNode->identifiers[2], rightValue);
        if (varNode->identifiers[1] == "sprite" && varNode->identifiers[2] == "z") {
          ECSCatalog::instance().setReorderRequired(true);
        }
      }
    }
      // 4 Otherwise, it's a system.entity.component.variable.
      // Ex: playerMovement.playerId.speed.x  = ...
    else if(varNode->numIdentifiers == 4) {
      System &system = ECSCatalog::instance().systemCatalog[varNode->identifiers[0]];
      if(system.hasVariable(varNode->identifiers[1])) {
        system.setVariable(varNode->identifiers[1], secondChild->returnValue.value);
        std::string identifierStr =  system.getVariableValue(varNode->identifiers[1]);
        int id = std::stoi(identifierStr);
        Entity &entity = ECSCatalog::instance().entityInstances[id];
        entity.setComponentVariable(varNode->identifiers[2], varNode->identifiers[3], rightValue);
        if (varNode->identifiers[2] == "sprite" && varNode->identifiers[3] == "z") {
          ECSCatalog::instance().setReorderRequired(true);
        }
      }
    }
  }

  void AstNodeAssign::runSystemAssign(AstNodeVariable * varNode) {
    std::string rightValue = parseSystemVariable(secondChild);
    System * currentSystem = ECSCatalog::instance().currentSystem;

    // 1. simple identifier: is my variable
    if(varNode->numIdentifiers == 1) {
      currentSystem->setVariable(varNode->identifiers[0], rightValue); // so we can retrieve if a function is called
      ECSCatalog::instance().systemCatalog[currentSystem->getName()].setVariable(varNode->identifiers[0], rightValue);
    }
    // 2. Double identifier: Must be a system.variable or a component variable for the current entity.
    else if(varNode->numIdentifiers == 2) {
        if ( currentSystem->hasComponent(varNode->identifiers[0])) {
          currentSystem->setCurrentEntityVariable(varNode->identifiers[0], varNode->identifiers[1], rightValue);
          if (varNode->identifiers[0] == "sprite" && varNode->identifiers[1] == "z") {
            ECSCatalog::instance().setReorderRequired(true);
          }
        } else {
          System &system = ECSCatalog::instance().systemCatalog[varNode->identifiers[0]];
          if(system.hasVariable(varNode->identifiers[1])) {
            system.setVariable(varNode->identifiers[1], rightValue);
          }
        }
    }
    // 3. Triple identifier: entity variable in system
    else if(varNode->numIdentifiers == 3) {
      // 3.1 Check if first part is any of my variables, so it's entity.variable
      if (currentSystem->hasVariable(varNode->identifiers[0])) {
        std::string identifierStr = currentSystem->getVariableValue(varNode->identifiers[0]);
        int id = std::stoi(identifierStr);
        Entity &entity = ECSCatalog::instance().entityInstances[id];
        entity.setComponentVariable(varNode->identifiers[1], varNode->identifiers[2], rightValue);
        if (varNode->identifiers[1] == "sprite" && varNode->identifiers[2] == "z") {
          ECSCatalog::instance().setReorderRequired(true);
        }
      }
    }
    // 4 Otherwise, it's a system.entity.component.variable.
    // Ex: playerMovement.playerId.speed.x  = ...
    else if(varNode->numIdentifiers == 4) {
      System &system = ECSCatalog::instance().systemCatalog[varNode->identifiers[0]];
      if(system.hasVariable(varNode->identifiers[1])) {
        system.setVariable(varNode->identifiers[1], secondChild->returnValue.value);
        std::string identifierStr =  system.getVariableValue(varNode->identifiers[1]);
        int id = std::stoi(identifierStr);
        Entity &entity = ECSCatalog::instance().entityInstances[id];
        entity.setComponentVariable(varNode->identifiers[2], varNode->identifiers[3], rightValue);
        if (varNode->identifiers[2] == "sprite" && varNode->identifiers[3] == "z") {
          ECSCatalog::instance().setReorderRequired(true);
        }
      }
    }
  }

  void AstNodeAssign::run() {
    if (ECSCatalog::instance().currentFunction != nullptr && ECSCatalog::instance().currentFunction->isValueReturned()) {
      return;
    }
    AstNode::run();

    if(ECSCatalog::instance().currentState == "function") {
      runFunctionAssign((AstNodeVariable *)firstChild);
    }

    if(ECSCatalog::instance().currentState == "system") {
      runSystemAssign((AstNodeVariable *)firstChild);
    }
  }

