#include "AstNodeWhile.h"

AstNodeWhile::AstNodeWhile(AstNode *fc) {
    firstChild = fc;
    secondChild = nullptr;
}

void AstNodeWhile::run() {
    condition->run();
    float conditionValue = std::stof(condition->returnValue.value);
    while (conditionValue > 0 && firstChild != nullptr) {
      firstChild->run();
      condition->run();
      conditionValue = std::stof(condition->returnValue.value);
    }
}