#include "AstNodeIfElse.h"

AstNodeIfelse::AstNodeIfelse(AstNode *fc, AstNode *sc) {
    firstChild = fc;
    secondChild = sc;
}

AstNodeIfelse::AstNodeIfelse(AstNode *fc) {
    firstChild = fc;
    secondChild = nullptr;
}

void AstNodeIfelse::run() {
    condition->run();
    float conditionValue = std::stof(condition->returnValue.value);
    if (fabs(conditionValue) > 0 && firstChild != nullptr) {
        firstChild->run();
    } else if (secondChild != nullptr) {
        secondChild->run();
    }
}
