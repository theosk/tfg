#include "AstNodeFunction.h"

void AstNodeFunction::run() {
  if (ECSCatalog::instance().currentFunction != nullptr && ECSCatalog::instance().currentFunction->isValueReturned()) {
    return;
  }

  if(ECSCatalog::instance().predefinedFunctionsCatalog.find(value.value) != ECSCatalog::instance().predefinedFunctionsCatalog.end()) {
    PredefinedFunction * func = ECSCatalog::instance().predefinedFunctionsCatalog[value.value];
    std::vector<ScriptVariable> variables;
    for (int i=0; i<arguments.size(); i++) {
      ScriptVariable currentVar = arguments[i];
      if(arguments[i].type == Variable) {
          if(ECSCatalog::instance().currentState == "system") {
              System * currentSystem = ECSCatalog::instance().currentSystem;
              currentVar.value = currentSystem->getVariableValue(currentVar.value);
          } else {
              Function * currentFunc = ECSCatalog::instance().currentFunction;
              currentVar.value = currentFunc->getVariableValue(currentVar.value);
          }
      }
      variables.push_back(currentVar);
    }
    returnValue = func->run(variables);

  } else if(ECSCatalog::instance().functionCatalog.find(value.value) != ECSCatalog::instance().functionCatalog.end()) {
    Function func = ECSCatalog::instance().functionCatalog[value.value];
    func.initArgumentVariables(arguments);
    returnValue = func.run();
  } else {
      std::cout << "function not found: " << value.value << std::endl;
  }
}