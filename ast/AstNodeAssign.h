#ifndef ASTNODE_ASSIGN_H
#define ASTNODE_ASSIGN_H

#include "AstNode.h"
#include "AstNodeVariable.h"

class AstNodeAssign: public AstNode {
public:
  AstNodeAssign(AstNode *fc, AstNode *sc);
  void runFunctionAssign(AstNodeVariable * varNode);
  void runSystemAssign(AstNodeVariable * varNode);
  void run();
};

#endif