#ifndef ASTNODE_IFELSE_H
#define ASTNODE_IFELSE_H

#include "AstNode.h"

class AstNodeIfelse: public AstNode {
public:
  AstNode * condition;
  AstNodeIfelse(AstNode *fc, AstNode *sc);
  AstNodeIfelse(AstNode *fc);
  void run() ;
};


#endif