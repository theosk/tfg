#include "AstNodeFor.h"

AstNodeFor::AstNodeFor(AstNode *fc, AstNode *sc) {
    firstChild = fc;
    secondChild = sc;
}

void AstNodeFor::run() {
    firstChild->run();
    condition->run();
    float conditionValue = std::stof(condition->returnValue.value);

    while (conditionValue > 0 && firstChild != nullptr) {
      condition->run();
      conditionValue = std::stof(condition->returnValue.value);
      secondChild->run();
    }
}