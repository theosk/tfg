#ifndef ASTNODE_WHILE_H
#define ASTNODE_WHILE_H

#include "AstNode.h"

class AstNodeWhile: public AstNode {
public:
  AstNode * condition;

  AstNodeWhile(AstNode *fc);
  void run();
};
#endif