#ifndef ASTNODE_INSTANCE_H
#define ASTNODE_INSTANCE_H

#include "AstNode.h"

class AstNodeInstance: public AstNode {
public:
  void run();
  AstNodeInstance(std::string value);
};


#endif