#include "AstNodeVariable.h"

void AstNodeVariable::run() {
    this->numIdentifiers = (identifiers[0] != "") + (identifiers[1] != "")+ (identifiers[2] != "")+ (identifiers[3] != "");
    value.value = identifiers[0];
    if (identifiers[1] != "")
      value.value += "." + identifiers[1];
    if (identifiers[2] != "")
      value.value += "." + identifiers[2];
    if (identifiers[3] != "")
      value.value += "." + identifiers[3];

    returnValue.value = "";
    if(ECSCatalog::instance().currentState == "function") {
      returnValue.value = parseFunctionVariable(this);
    }
    else if(ECSCatalog::instance().currentState == "system") {
      returnValue.value = parseSystemVariable(this);
    } else {
      returnValue.value = "0.0";
    }
}