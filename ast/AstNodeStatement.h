#ifndef ASTNODE_STATEMENT_H
#define ASTNODE_STATEMENT_H

#include "AstNode.h"

class AstNodeStatement: public AstNode {
public:
  AstNodeStatement(AstNode *fc, AstNode *sc);
  void run();
};

#endif