#ifndef ASTNODE_INSTANCE_DELETE_H
#define ASTNODE_INSTANCE_DELETE_H

#include "AstNode.h"

class AstNodeInstanceDelete: public AstNode {
public:
  AstNodeInstanceDelete(std::string value);
  void run();
};

#endif