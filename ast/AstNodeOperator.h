#ifndef ASTNODE_ARITHMETIC_H
#define ASTNODE_ARITHMETIC_H

#include "AstNode.h"

class AstNodeOperator: public AstNode {
public:

    AstNodeOperator(AstNode *fc, AstNode *sc);
    AstNodeOperator(AstNode *fc, AstNode *sc, std::string op);
    void run();
  protected:
    float firstVal = 0.0;
    float secondVal = 0.0;
};

#endif