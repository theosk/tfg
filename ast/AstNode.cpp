#include "AstNode.h"

AstNode::AstNode() {
    firstChild = nullptr;
    secondChild = nullptr;
}

 void AstNode::run() {
      if(firstChild != nullptr) {
        firstChild->run();
      }
      if(secondChild != nullptr) {
        secondChild->run();
      }
  } 

std::string AstNode::parseFunctionVariable(AstNode * node) {
    if(node->numIdentifiers == 1) {
      return ECSCatalog::instance().currentFunction->getVariableValue(node->identifiers[0]);
    }
    // 2. Double identifier: Must be a system.variable.
    else if(node->numIdentifiers == 2) {
        System &system = ECSCatalog::instance().systemCatalog[node->identifiers[0]];
        return system.getVariableValue(node->identifiers[1]);
    }
    
    // 3. Triple identifier: entity variable in function
    // Ex: a.speed.x = ...
    else if(node->numIdentifiers == 3) {
      // 3.1 Check if first part is any of my variables, so it's entity.variable
      if (ECSCatalog::instance().currentFunction->hasVariable(node->identifiers[0])) {
        std::string identifierStr = ECSCatalog::instance().currentFunction->getVariableValue(node->identifiers[0]);
        if (identifierStr != "") {
          int id = std::stoi(identifierStr);
          Entity &entity = ECSCatalog::instance().entityInstances[id];
          return entity.getComponentVariable(node->identifiers[1], node->identifiers[2]).value;
        }
      }
    }
      // 4 Otherwise, it's a system.entity.component.variable.
      // Ex: playerMovement.playerId.speed.x  = ...
    else if(node->numIdentifiers == 4) {
      System &system = ECSCatalog::instance().systemCatalog[node->identifiers[0]];
      if(system.hasVariable(node->identifiers[1])) {
        std::string identifierStr =  system.getVariableValue(node->identifiers[1]);
        int id = std::stoi(identifierStr);
        Entity &entity = ECSCatalog::instance().entityInstances[id];
        return entity.getComponentVariable(node->identifiers[2], node->identifiers[3]).value;
      }
    }

    if (!node->returnValue.value.empty()) {
      return node->returnValue.value;
    }
    return "0.0";
}

std::string AstNode::parseSystemVariable(AstNode * node) {
    if(node->numIdentifiers == 1) {
      if (node->identifiers[0] == "id") {
        ScriptVariable var;
        return std::to_string(ECSCatalog::instance().currentSystem->getCurrentEntityId());
      }
      return ECSCatalog::instance().currentSystem->getVariableValue(node->identifiers[0]);
    }

    // Can belong to entity (component.variable) or system (system.variable)
    if(node->numIdentifiers == 2) {
      int id = ECSCatalog::instance().currentSystem->getCurrentEntityId();
      if (ECSCatalog::instance().entityInstances[id].hasComponent(node->identifiers[0])){
        ScriptVariable &var = ECSCatalog::instance().currentSystem->getCurrentEntityVariable(node->identifiers[0], node->identifiers[1]); 
        return var.value;
    } else {
        ScriptVariable &var = ECSCatalog::instance().systemCatalog[node->identifiers[0]].getVariable(node->identifiers[1]);
        return var.value != "" ? var.value : "0";
      }
    }

    if(node->numIdentifiers == 3) { 
      // Check if first part is any of my variables, so it's entity.variable
      if (ECSCatalog::instance().currentSystem->hasVariable(node->identifiers[0])) {
        std::string identifierStr = ECSCatalog::instance().currentSystem->getVariableValue(node->identifiers[0]);
        int id = std::stoi(identifierStr);
        Entity &entity = ECSCatalog::instance().entityInstances[id];
        ScriptVariable var = entity.getComponentVariable(node->identifiers[1], node->identifiers[2]);
        return var.value;
      }
    }

    if (!node->returnValue.value.empty()) {
      return node->returnValue.value;
    }
    return "0.0";
}