#include "AstNodeInstanceDelete.h"

AstNodeInstanceDelete::AstNodeInstanceDelete(std::string value) {
    ScriptVariable var;
    var.type = DataType::String;
    var.name = "entityIdentifier";
    var.value = value;
    this->value = var;
}


void AstNodeInstanceDelete::run() {
    std::string strValue;

    if(ECSCatalog::instance().currentState == "function") {
      strValue = ECSCatalog::instance().currentFunction->getVariableValue(value.value);
    }
    if(ECSCatalog::instance().currentState == "system") {
      strValue = ECSCatalog::instance().currentSystem->getVariableValue(value.value);
    }
    int id = ::atoi(strValue.c_str());
    ECSCatalog::instance().entitiesToDelete.push_back(id);
}