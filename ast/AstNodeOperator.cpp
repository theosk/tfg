#include "AstNodeOperator.h"

AstNodeOperator::AstNodeOperator(AstNode *fc, AstNode *sc) {
    firstChild = fc;
    secondChild = sc;
}

AstNodeOperator::AstNodeOperator(AstNode *fc, AstNode *sc, std::string op) {
    firstChild = fc;
    secondChild = sc;
    this->op = op;
}


void AstNodeOperator::run() {
    AstNode::run();     
    if(firstChild != nullptr) {
        std::string firstValStr = firstChild->returnValue.value;
        firstVal = std::stof(firstValStr);
    }
    if (secondChild != nullptr) {
        std::string secondValStr = secondChild->returnValue.value;
        secondVal = std::stof(secondValStr);
    }

    if(this->op == "+")
        returnValue.value = std::to_string(firstVal+secondVal);
    else if(this->op == "-")
        returnValue.value = std::to_string(firstVal-secondVal);
    else if(this->op == "*")
        returnValue.value = std::to_string(firstVal*secondVal);
    else if(this->op == "/")
        returnValue.value = std::to_string(firstVal/secondVal);
    else if(this->op == ">")
        returnValue.value = firstVal>secondVal ? "1" : "0";
    else if(this->op == "<")
        returnValue.value = firstVal<secondVal ? "1" : "0";
    else if(this->op == ">=")
        returnValue.value = firstVal>=secondVal ? "1" : "0";
    else if(this->op == "<=")
        returnValue.value = firstVal<=secondVal ? "1" : "0";
    else if(this->op == "==")
        returnValue.value = firstVal==secondVal ? "1" : "0";
    else if(this->op == "!=")
        returnValue.value = firstVal==secondVal ? "0" : "1";
    else if(this->op == "&&")
        returnValue.value = firstVal&&secondVal ? "1" : "0";
    else if(this->op == "||")
        returnValue.value = firstVal||secondVal ? "1" : "0";        
    else if(this->op == "%")
        returnValue.value = std::to_string(fmod(firstVal,secondVal));
}
