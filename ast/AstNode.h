#ifndef ASTNODE_HH
#define ASTNODE_HH
#include "../scriptVariable.h"
#include "../ecs/entity.h"
#include "../ecs/component.h"
#include "../ecs/system.h"
#include "../ecs/function.h"
#include "../ecs/catalog.h"
#include <cmath>
#include <string>
#include <iostream>

const int MAX_IDENTIFIERS = 4;

class AstNode {
public:
  int nodeType;
  ScriptVariable value;
  ScriptVariable returnValue;
  std::string identifiers[MAX_IDENTIFIERS];
  std::string op;
  int numIdentifiers;
  AstNode * firstChild;
  AstNode * secondChild;

  AstNode();
  virtual void run();
  std::string parseFunctionVariable(AstNode * node);
  std::string parseSystemVariable(AstNode * node);

};

#endif
