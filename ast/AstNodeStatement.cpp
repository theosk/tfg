#include "AstNodeStatement.h"

AstNodeStatement::AstNodeStatement(AstNode *fc, AstNode *sc) {
    firstChild = fc;
    secondChild = sc;
}

void AstNodeStatement::run() {
    AstNode::run();
}