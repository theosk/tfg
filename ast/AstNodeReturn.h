#ifndef ASTNODE_RETURN_H
#define ASTNODE_RETURN_H

#include "AstNode.h"

class AstNodeReturn: public AstNode {
public:
  AstNodeReturn(AstNode *fc);
  AstNodeReturn();
  void run() ;
};


#endif