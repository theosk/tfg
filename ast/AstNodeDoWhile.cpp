#include "AstNodeDoWhile.h"
  
AstNodeDoWhile::AstNodeDoWhile(AstNode *fc) {
    firstChild = fc;
    secondChild = nullptr;
}

void AstNodeDoWhile::run() {
    condition->run();
    float conditionValue = std::stof(condition->returnValue.value);
    do {
      firstChild->run();
      condition->run();
      conditionValue = std::stof(condition->returnValue.value);
    } while (conditionValue > 0 && firstChild != nullptr) ;
}