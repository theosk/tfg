#include "AstNodeInstance.h"

AstNodeInstance::AstNodeInstance(std::string value) {
    ScriptVariable var;
    var.type = DataType::String;
    var.name = "entityName";
    var.value = value;
    this->value = var;  
}


void AstNodeInstance::run() {
    int id = ECSCatalog::instance().addEntityInstance(value.value);
    Entity &e = ECSCatalog::instance().entityInstances[id];
    for (auto & it:ECSCatalog::instance().systemCatalog) {
      if(e.hasSystem(it.second.getName()))
        it.second.addEntity(id);
    }

    returnValue.value = std::to_string(id);
}
