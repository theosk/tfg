#ifndef ASTNODE_DOWHILE_H
#define ASTNODE_DOWHILE_H

#include "AstNode.h"

class AstNodeDoWhile: public AstNode {
public:
  AstNode * condition;
  AstNodeDoWhile(AstNode *fc);
  void run();
};

#endif