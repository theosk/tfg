#ifndef ASTNODE_FOR_H
#define ASTNODE_FOR_H

#include "AstNode.h"

class AstNodeFor: public AstNode {
public:
  AstNode * condition;

  AstNodeFor(AstNode *fc, AstNode *sc);
  void run();
};
#endif