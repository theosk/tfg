#include "AstNodeReturn.h"

AstNodeReturn::AstNodeReturn() {
    firstChild = nullptr;
    secondChild = nullptr;
}

AstNodeReturn::AstNodeReturn(AstNode *fc) {
    firstChild = fc;
    secondChild = nullptr;
}

void AstNodeReturn::run() {
    returnValue.value = "0";
    if (firstChild != nullptr) {
        firstChild->run();
        returnValue.value = firstChild->returnValue.value;
    }

    ECSCatalog::instance().setReturnedValue(returnValue);

}
