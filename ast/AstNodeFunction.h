#ifndef ASTNODE_FUNCTION_H
#define ASTNODE_FUNCTION_H

#include "AstNode.h"
#include <vector> 

class AstNodeFunction: public AstNode {
public:
  std::vector <ScriptVariable> arguments;
  void run();
};

#endif